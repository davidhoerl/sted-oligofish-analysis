library(knitr)
opts_chunk$set(
  concordance=TRUE
)

  
install.packages("tidyverse")
install.packages("ggplot2")
library(ggplot2)
library(dplyr)
library(ggthemes)
library(stringr)

# get the mode of kde of x
density_mode = function(x)
{
  den = density(x, bw='SJ')
  den$x[which.max(den$y)]
}


#read data 
#import compaction_sted_data
#combined_analysis_2d
data_2d = read.csv(file.choose(), header=T)
#combined_analysis_3d
data_3d = read.csv(file.choose(), header=T)

#2D data
data_2d = filter(data_2d, len2d < .25)

#3D data
data_3d = filter(data_3d, len3d < .25, X.acquisition.sted_mode == "3d")

#Summary of 2D data
summary_all_2d = summarise(group_by(data_2d, interaction(X.experiment.cell_type, X.experiment.condition),
                        interaction(X.experiment.targets.0, X.experiment.targets.1)),  
                        median = median(len2d*1000), mode = density_mode(len2d*1000), mean = mean(len2d*1000),
                        mean3destim =  mean(len2d*1000) / (pi/4), median3destim =  median(len2d*1000) / (pi/4))%>% as.data.frame()

summary_samplesize_2d = summarise(group_by(data_2d, interaction(X.experiment.cell_type, X.experiment.condition),
                                        interaction(X.experiment.targets.0, X.experiment.targets.1, X.preparation.location)), 
                               samplesize = length(file), nreplicates = length(unique(interaction(slide_group_id, slide_id))))

# Median lengths and sample sizes for 3D Data
summary_all_3d = summarise(group_by(data_3d, interaction(X.experiment.cell_type, X.experiment.condition, X.acquisition.sted_mode),
                          interaction(X.experiment.targets.0, X.experiment.targets.1)),median = median(len3d*1000), 
                           mode = density_mode(len3d*1000), mean = mean(len3d*1000), sd=sd(len3d*1000)) %>% as.data.frame()

summary_samplesize_3d = summarise(group_by(data_3d, interaction(X.experiment.cell_type, X.experiment.condition, X.acquisition.sted_mode),
                                          interaction(X.experiment.targets.0, X.experiment.targets.1)), 
                                 samplesize = length(file), nreplicates = length(unique(interaction(slide_group_id, slide_id))))
  

#2D DATA
#Plot 2D data as boxplot and cumulative distribution
all_inactive_2d <- data_2d %>%
                filter(str_detect(X.experiment.targets.0, "dead"))

all_inactive_2d$group <- "inactive"

InactiveAB_2d <- subset(all_inactive_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11B" & X.experiment.targets.1 == "dead11A")
InactiveBC_2d <- subset(all_inactive_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11B" & X.experiment.targets.1 == "dead11C")
InactiveCD_2d <- subset(all_inactive_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11D" & X.experiment.targets.1 == "dead11C")
InactiveDE_2d <- subset(all_inactive_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11D" & X.experiment.targets.1 == "dead11E")

all_active_2d <- data_2d %>%
  filter(str_detect(X.experiment.targets.0, "alive"))

all_active_2d$group <- "active"

ActiveAB_2d <- subset(all_active_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3B" & X.experiment.targets.1 == "alive3A")
ActiveBC_2d <- subset(all_active_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3B" & X.experiment.targets.1 == "alive3C")
ActiveCD_2d <- subset(all_active_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3D" & X.experiment.targets.1 == "alive3C")
ActiveDE_2d <- subset(all_active_2d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3D" & X.experiment.targets.1 == "alive3E")


#Plot boxplot
ggplot(all_active_2d) + 
  geom_boxplot(size= 0.5, notch = F, notchwidth = 0.8, outlier.alpha = 0.5, varwidth = F, aes(y=len2d * 1e3, x=interaction(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1), lex.order = T), fill= interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) + 
  ggtitle('Active') + 
  ylab('Projected distance [nm]') +
  xlab('Interval') +
  theme_classic() + scale_fill_manual(values=c("red3", "red3", "red3", "red3")) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(fill='Interval')

ggplot(all_inactive_2d) + 
  geom_boxplot(size= 0.5, notch = F, notchwidth = 0.8, outlier.alpha = 0.5, varwidth = F, aes(y=len2d * 1e3, x=interaction(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1), lex.order = T), fill= interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) + 
  ggtitle('Inactive') + 
  ylab('Projected distance [nm]') +
  xlab('Interval') +
  theme_classic() + scale_fill_manual(values=c("royalblue", "royalblue", "royalblue", "royalblue")) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(fill='Interval')

#Plot cumulative distribution
plot(ecdf(all_active_2d$len2d*1e3),
     xlab="Projected distance [nm]",
     ylab="Cumulative Proportion",
     main="Cumulative distribution",
     col="red3",
     cex = 0.3,
     lwd=3)
lines(ecdf(all_inactive_2d$len2d*1e3), col="royalblue", cex=0.5, lwd=3)

legend('bottomright', 
       legend=c("K562 Active AB-BC-CD-DE","K562 Inactive AB-BC-CD-DE"),  
       col=c("red3","royalblue"),  
       pch=15)


# Statistical tests
# all pairwise with correction (single pairs commented out below)
all_active_2d$targets = str_c(all_active_2d$X.experiment.targets.1, all_active_2d$X.experiment.targets.0, sep = '_')
pairwise.wilcox.test(all_active_2d$len2d, all_active_2d$targets, p.adjust.method="holm")

all_inactive_2d$targets = str_c(all_inactive_2d$X.experiment.targets.1, all_inactive_2d$X.experiment.targets.0, sep = '_')
pairwise.wilcox.test(all_inactive_2d$len2d, all_inactive_2d$targets, p.adjust.method="holm")


# #Statistical tests for 2D data sets
# #Inactive
# #1: Test, if Inactive AB and Inactive BC are significantly different
# AB_BC<- rbind(InactiveAB_2d,InactiveBC_2d)
# pairwise.wilcox.test(AB_BC$len2d, AB_BC$X.experiment.targets.1, p.adjust.method="holm")
# 
# #2: Test, if Inactive AB and Inactive CD are significantly different
# AB_CD<- rbind(InactiveAB_2d,InactiveCD_2d)
# pairwise.wilcox.test(AB_CD$len2d, AB_CD$X.experiment.targets.1, p.adjust.method="holm")
# 
# #3: Test, if Inactive AB and Inactive DE are significantly different
# AB_DE<- rbind(InactiveAB_2d,InactiveDE_2d)
# pairwise.wilcox.test(AB_DE$len2d, AB_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #4: Test, if Inactive BC and Inactive CD are significantly different
# BC_CD<- rbind(InactiveBC_2d,InactiveCD_2d)
# pairwise.wilcox.test(BC_CD$len2d, BC_CD$X.experiment.targets.0, p.adjust.method="holm")
# 
# #5: Test, if Inactive BC and Inactive DE are significantly different
# BC_DE<- rbind(InactiveBC_2d,InactiveDE_2d)
# pairwise.wilcox.test(BC_DE$len2d, BC_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #6: Test, if Inactive CD and Inactive DE are significantly different
# CD_DE<- rbind(InactiveCD_2d,InactiveDE_2d)
# pairwise.wilcox.test(CD_DE$len2d, CD_DE$X.experiment.targets.1, p.adjust.method="holm")


# #Active
# #1: Test, if Active AB and Active BC are significantly different
# AB_BC<- rbind(ActiveAB_2d,ActiveBC_2d)
# pairwise.wilcox.test(AB_BC$len2d, AB_BC$X.experiment.targets.1, p.adjust.method="holm")
# 
# #2: Test, if Active AB and Active CD are significantly different
# AB_CD<- rbind(ActiveAB_2d,ActiveCD_2d)
# pairwise.wilcox.test(AB_CD$len2d, AB_CD$X.experiment.targets.1, p.adjust.method="holm")
# 
# #3: Test, if Active AB and Active DE are significantly different
# AB_DE<- rbind(ActiveAB_2d,ActiveDE_2d)
# pairwise.wilcox.test(AB_DE$len2d, AB_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #4: Test, if Active BC and Active CD are significantly different
# BC_CD<- rbind(ActiveBC_2d,ActiveCD_2d)
# pairwise.wilcox.test(BC_CD$len2d, BC_CD$X.experiment.targets.0, p.adjust.method="holm")
# 
# #5: Test, if Active BC and Active DE are significantly different
# BC_DE<- rbind(ActiveBC_2d,ActiveDE_2d)
# pairwise.wilcox.test(BC_DE$len2d, BC_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #6: Test, if Active CD and Active DE are significantly different
# CD_DE<- rbind(ActiveCD_2d,ActiveDE_2d)
# pairwise.wilcox.test(CD_DE$len2d, CD_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #Test all_inactive_2d vs. all_active_2d
# total_2d <- rbind(all_active_2d, all_inactive_2d)
# pairwise.wilcox.test(total_2d$len2d, total_2d$group, p.adjust.method="holm")


#3D DATA
# Filter and organize 3D data
all_inactive_3d <- data_3d %>%
  filter(str_detect(X.experiment.targets.0, "dead"))

all_inactive_3d$group <- "inactive"

InactiveAB_3d <- subset(all_inactive_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11B" & X.experiment.targets.1 == "dead11A")
InactiveBC_3d <- subset(all_inactive_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11B" & X.experiment.targets.1 == "dead11C")
InactiveCD_3d <- subset(all_inactive_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11D" & X.experiment.targets.1 == "dead11C")
InactiveDE_3d <- subset(all_inactive_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "dead11D" & X.experiment.targets.1 == "dead11E")

all_active_3d <- data_3d %>%
  filter(str_detect(X.experiment.targets.0, "alive"))

all_active_3d$group <- "active"

ActiveAB_3d <- subset(all_active_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3B" & X.experiment.targets.1 == "alive3A")
ActiveBC_3d <- subset(all_active_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3B" & X.experiment.targets.1 == "alive3C")
ActiveCD_3d <- subset(all_active_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3D" & X.experiment.targets.1 == "alive3C")
ActiveDE_3d <- subset(all_active_3d, X.experiment.cell_type == "k562" & X.experiment.targets.0 == "alive3D" & X.experiment.targets.1 == "alive3E")


# Plot 3D data as histogram

#Inactive
ggplot(InactiveAB_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="royalblue", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")

ggplot(InactiveBC_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="royalblue", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")

ggplot(InactiveCD_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="royalblue", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")

ggplot(InactiveDE_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="royalblue", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")


#Active
ggplot(ActiveAB_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                    color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="red3", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")

ggplot(ActiveBC_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="red3", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")

ggplot(ActiveCD_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="red3", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")

ggplot(ActiveDE_3d, aes(x=len3d * 1e3, fill=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1),
                     color=interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1))) +
  geom_histogram(aes(y=..count../sum(..count..)), alpha=1, binwidth = 13, fill="red3", color="Black") +
  ylim(-0.035,0.25)+
  xlim(-10,260)+
  facet_grid(interaction(X.experiment.cell_type, X.experiment.targets.0, X.experiment.targets.1)~.)+
  xlab('End-to-end distance [nm]') +
  ylab('Relative frequency') +
  geom_point(aes(x=mean(len3d*1e3),y=-0.025),size=2,color="red")+
  geom_errorbarh(aes(y=-0.025,xmax = mean(len3d*1e3)+sd(len3d*1e3), xmin = mean(len3d*1e3)-sd(len3d*1e3)),height=0.02, size=1,color="red") +
  theme(legend.position = "none")


# Statistical tests
# all pairwise with correction (single pairs commented out below)
all_active_3d$targets = str_c(all_active_3d$X.experiment.targets.1, all_active_3d$X.experiment.targets.0, sep = '_')
pairwise.wilcox.test(all_active_3d$len3d, all_active_3d$targets, p.adjust.method="holm")

all_inactive_3d$targets = str_c(all_inactive_3d$X.experiment.targets.1, all_inactive_3d$X.experiment.targets.0, sep = '_')
pairwise.wilcox.test(all_inactive_3d$len3d, all_inactive_3d$targets, p.adjust.method="holm")


# #Statistical tests for 3D data sets
# #Inactive
# #1: Test, if Inactive AB and Inactive BC are significantly different
# AB_BC<- rbind(InactiveAB_3d,InactiveBC_3d)
# pairwise.wilcox.test(AB_BC$len3d, AB_BC$X.experiment.targets.1, p.adjust.method="holm")
# 
# #2: Test, if Inactive AB and Inactive CD are significantly different
# AB_CD<- rbind(InactiveAB_3d,InactiveCD_3d)
# pairwise.wilcox.test(AB_CD$len3d, AB_CD$X.experiment.targets.1, p.adjust.method="holm")
# 
# #3: Test, if Inactive AB and Inactive DE are significantly different
# AB_DE<- rbind(InactiveAB_3d,InactiveDE_3d)
# pairwise.wilcox.test(AB_DE$len3d, AB_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #4: Test, if Inactive BC and Inactive CD are significantly different
# BC_CD<- rbind(InactiveBC_3d,InactiveCD_3d)
# pairwise.wilcox.test(BC_CD$len3d, BC_CD$X.experiment.targets.0, p.adjust.method="holm")
# 
# #5: Test, if Inactive BC and Inactive DE are significantly different
# BC_DE<- rbind(InactiveBC_3d,InactiveDE_3d)
# pairwise.wilcox.test(BC_DE$len3d, BC_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #6: Test, if Inactive CD and Inactive DE are significantly different
# CD_DE<- rbind(InactiveCD_3d,InactiveDE_3d)
# pairwise.wilcox.test(CD_DE$len3d, CD_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# 
# #Active
# #1: Test, if Active AB and Active BC are significantly different
# AB_BC<- rbind(ActiveAB_3d,ActiveBC_3d)
# pairwise.wilcox.test(AB_BC$len3d, AB_BC$X.experiment.targets.1, p.adjust.method="holm")
# 
# #2: Test, if Active AB and Active CD are significantly different
# AB_CD<- rbind(ActiveAB_3d,ActiveCD_3d)
# pairwise.wilcox.test(AB_CD$len3d, AB_CD$X.experiment.targets.1, p.adjust.method="holm")
# 
# #3: Test, if Active AB and Active DE are significantly different
# AB_DE<- rbind(ActiveAB_3d,ActiveDE_3d)
# pairwise.wilcox.test(AB_DE$len3d, AB_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #4: Test, if Active BC and Active CD are significantly different
# BC_CD<- rbind(ActiveBC_3d,ActiveCD_3d)
# pairwise.wilcox.test(BC_CD$len3d, BC_CD$X.experiment.targets.0, p.adjust.method="holm")
# 
# #5: Test, if Active BC and Active DE are significantly different
# BC_DE<- rbind(ActiveBC_3d,ActiveDE_3d)
# pairwise.wilcox.test(BC_DE$len3d, BC_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #6: Test, if Active CD and Active DE are significantly different
# CD_DE<- rbind(ActiveCD_3d,ActiveDE_3d)
# pairwise.wilcox.test(CD_DE$len3d, CD_DE$X.experiment.targets.1, p.adjust.method="holm")
# 
# #Test, all_inactive_3d vs. all_active_3d
# total_3d <- rbind(all_active_3d, all_inactive_3d)
# pairwise.wilcox.test(total_3d$len3d, total_3d$group, p.adjust.method="holm")

