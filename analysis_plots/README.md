# Analysis and plots

This directory contains mostly notebooks for generating plots or analysis from the tables produced by the main pipeline.

It contains:
* code for production plots of distances, e.g. histograms/box/swarmplots in ```distance_plots.ipynb```
* code for producing QQ-plots to compare the distance distributions of various combinations of measurements in ```qqplots.ipynb```
* preliminary code for combining DNAse/MNAse measurements with our data in ```combine_omics_fish.ipynb```