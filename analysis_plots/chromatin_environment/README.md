# Chromatin environment of single FISH spots
(for Sup. Fig. 4)

This directory contains:
* an ImageJ macro ```make_overlay.ijm``` to overlay the max-projected FISH signal onto the SiR-channel and adjust pixel sizes
* an ImageJ macro ```extract_nucleus_and_spot``` to help annotating spot location and nuclear outlines. It will save positions, nuclear mask and a background-subtracted version of the SiR channel.
* ```fish_sir_quantile.ipynb``` a notebook to determine quantiles at FISH spot locations and do plots/significance tests.