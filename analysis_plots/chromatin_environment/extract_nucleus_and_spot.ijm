dirToProcess = getDirectory("Choose to directory to process.");
files = getFileList(dirToProcess);

// close everything 
run("Close All");
run("Clear Results");

for (i=0; i<files.length; i++)
{

	// close everything 
	run("Close All");
	run("Clear Results");
	
	file = files[i];

	// ignore non-TIFF files
	if (!endsWith(file, '.tif'))
	{
		continue;
	}

	// open file
	open(dirToProcess + file);

	Stack.setChannel(2);
	resetMinAndMax();
	
	// ask user to outline nucleus and save mask
	setTool("freehand");
	waitForUser("1. outline the nucleus (select nothing to skip)");

	if (selectionType() == -1)
	{
		continue;
	}
	run("Create Mask");

	// ask user to select point
	// save as single-line csv
	selectWindow(file);
	run("Select None");

	Stack.setChannel(1);
	resetMinAndMax();
	
	setTool("point");
	waitForUser("2. mark FISH spot (select nothing to skip)");
	if (selectionType() == -1)
	{
		continue;
	}

	// make output dir
	if (!File.exists(dirToProcess + file + "res"))
	{
		File.makeDirectory(dirToProcess + file + "res");
	}

	// save mask
	selectWindow("Mask");
	save(dirToProcess + file + "res" + File.separator + "mask.tif");

	// save point
	selectWindow(file);
	roiManager("add");
	roiManager("List");
	saveAs("Results", dirToProcess + file + "res" + File.separator + "point.csv");
	roiManager("reset");
	
	// do a simple background subtraction
	run("Split Channels");
	selectWindow("C2-" + file);
	run("Subtract Background...", "rolling=50");
	save(dirToProcess + file + "res" + File.separator + "bgsub.tif");

	// close everything 
	run("Close All");
	run("Clear Results");
}
