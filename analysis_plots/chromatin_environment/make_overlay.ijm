directory = getDirectory("Choose to directory to process.");

// for old channel names & pixel sizes
function make_overlay_old(directory) {
	run("Close All");
	
	files = getFileList(directory);
	for (i=0; i<files.length; i++)
	{
		file = files[i];
	
		if (!endsWith(file, ".msr"))
		{
			continue;
		}
		
		run("Bio-Formats Importer", "open=" + directory + file+" color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_1 series_2");
		
		// spot: make 32bit, maxproject, close original
		selectWindow(file + " - ExpControl Ch1 {1}");
		run("32-bit");
		run("Z Project...", "projection=[Max Intensity]");
		selectWindow(file +  " - ExpControl Ch1 {1}");
		close();
	
		// make new image, copy max projection there
		newImage("Untitled", "32-bit black", 750, 750, 1);
		selectWindow("MAX_"+file+ " - ExpControl Ch1 {1}");
		run("Copy");
		selectWindow("Untitled");
		run("Paste");
		selectWindow("MAX_"+file+ " - ExpControl Ch1 {1}");
		close();	
	
		// scale sir image, close original
		selectWindow(file + " - ExpControl Ch1 {0}");
		run("32-bit");
		run("Scale...", "x=1.5 y=1.5 width=750 height=750 interpolation=Bilinear average create");
		selectWindow(file + " - ExpControl Ch1 {0}");
		close();
	
		run("Merge Channels...", "c1=Untitled c2=["+file+" - ExpControl Ch1 {0}-1] create");
		saveAs("Tiff", directory + file + "OVERLAY.tif");
		close();	
	}
}

// for NEW channel names & pixel sizes
function make_overlay_new(directory) {
	run("Close All");
	
	files = getFileList(directory);
	for (i=0; i<files.length; i++)
	{
		file = files[i];
	
		if (!endsWith(file, ".msr"))
		{
			continue;
		}
		
		run("Bio-Formats Importer", "open=" + directory + file+" color_mode=Default rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT series_1 series_2");
		
		// spot: make 32bit, maxproject, close original
		selectWindow(file + " - ExpControl Ch1 {0}");
		run("32-bit");
		run("Z Project...", "projection=[Max Intensity]");
		selectWindow(file +  " - ExpControl Ch1 {0}");
		close();

		// scale spot image
		selectWindow("MAX_"+file+ " - ExpControl Ch1 {0}");
		run("Scale...", "x=1.5 y=1.5 width=101 height=101 interpolation=Bilinear average create");
		selectWindow("MAX_"+file+ " - ExpControl Ch1 {0}");
		close();
	
		// make new image, copy max projection there
		newImage("Untitled", "32-bit black", 500, 500, 1);
		selectWindow("MAX_"+file+ " - ExpControl Ch1 {0}-1");
		run("Copy");
		selectWindow("Untitled");
		run("Paste");
		selectWindow("MAX_"+file+ " - ExpControl Ch1 {0}-1");
		close();	
	
		// scale sir image, close original
		selectWindow(file + " - ExpControl Ch1 {1}");
		run("32-bit");
	
		run("Merge Channels...", "c1=Untitled c2=["+file+" - ExpControl Ch1 {1}] create");
		saveAs("Tiff", directory + file + "OVERLAY.tif");
		close();	
	}
}

setBatchMode(true);
make_overlay_new(directory);
print("DONE");
setBatchMode(false);