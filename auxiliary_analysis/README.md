# Auxiliary analysis

This directory contains notebooks that are more concerned with technical aspects of the datasets and quality control.

* ```cell_volume_determination.ipynb``` contains code for segmenting cells in spinning disk images to determine nuclear volume of various cell types
* ```check_quality_zlocation.ipynb``` contains code for assessing image quality (good/bad classification) dependent on z-location from metadata
* ```classification_error_inspection.ipynb``` contains snippets to save false positives/negatives of the new HDF5-dataset based classification
* ```compare_localization_runs.ipynb``` contains code to compare localization runs on the same data
* ```get_microscope_metadata.ipynb``` contain code to extract microscope metadata
* ```localization_correlation_plots.ipynb``` contains snippets for joint plotting of various localization results, e.g. locations in different dimensions
* ```psf_extraction.ipynb``` contains code for extracting PSFs from datasets
* ```sample_age_quality.ipynb``` contains code for correlating image dataset quality (% good images) and sample age
* ```spot_intensity_analysis.ipynb``` contains code for estimating the number of flurophores in a FISH spot
* ```bead_timeseries_localization_precision.ipynb``` contains code for empirically estimating the localization precision of our microscope from repeated measurements of the same fluorescent beads 
