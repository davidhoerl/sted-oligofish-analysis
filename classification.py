import numpy as np
from scipy.ndimage import gaussian_filter, median_filter, gaussian_laplace, sobel
from skimage.feature import peak_local_max
from scipy.spatial import kdtree
import h5py as h5


def get_h5_experiment_name(lvl):
    return '_'.join([l+idx for l, idx in zip(*([iter(lvl)] * 2))])


def getfeatures_h5(h5file, levels, root='experiment', configs_and_channels=((0,0),(0,1))):

    # if we are given a string (filepath) instead of an initialized
    # h5py.File, we open it ouselves
    needs_closing = False
    if not isinstance(h5file, h5.File):
        needs_closing = True
        h5file = h5.File(h5file, 'r')

    idx = get_h5_experiment_name(levels)
    res = []
    for (config, channel) in configs_and_channels:
        # TODO: handle nonexistent images?
        img = np.squeeze(np.array(h5file[root][idx][str(config)][str(channel)]))
        res_i = getfeatures_single(img)
        if res_i is None:
            return None
        res += res_i

    # we opened the hdf5 file in this function call -> close it again
    if (needs_closing):
        h5file.close()

    return res

# static version id
getfeatures_h5.version = 1

def getfeatures_single(img, halflifefactor = 0.5):

    # make sure image is float and detect peaks in smoothed version
    img = img.astype(np.float)
    g1 = gaussian_filter(img, 1)
    p1 = peak_local_max(g1, min_distance=2, exclude_border=False)

    # no! local maxima -> completely uniform img
    if len(p1) == 0:
        return None

    # get sorted (intensity, maxima_index) lists
    p1i = sorted([(img[tuple(p1[i,:])], i) for i in range(len(p1))], key=lambda x: x[0], reverse=True)

    # get "halflife" := number of spots that have at least i(brightest spot) * halflifefactor
    halflife1 = sum([p1i[i][0] > halflifefactor * p1i[0][0] for i in range(len(p1i))])

    p1good = [p1[p1i[i][1]] for i in range(halflife1)]

    # "global" features: mean, var, halflifes
    features = [np.mean(img), np.var(img), halflife1]

    # "local" features at the brightst points in both channels
    for sigma in [0.7, 1, 1.5, 2.25, 3.5, 5]:
        features.append(gaussian_filter(img, sigma)[tuple(p1good[0])])
        features.append(gaussian_laplace(img, sigma)[tuple(p1good[0])])

    # some more local features without sigma dependence
    features.append(sobel(img)[tuple(p1good[0])])
    features.append(img[tuple(p1good[0])])

    return [float(f) for f in features]


def getfeatures(im):
    
    i1 = im[:,:,0]
    g1 = gaussian_filter(i1, 1)
    i2 = im[:,:,1]
    g2 = gaussian_filter(i2, 1)
    p1 = peak_local_max(g1, min_distance=2)
    p2 = peak_local_max(g2, min_distance=2)

    # no! local maxima -> completely uniform img
    # the following features no longer make sense, we handle this error outside
    if len(p1) == 0 or len(p2) == 0:
        return None
    
    p1i = sorted([(i1[p1[i,0], p1[i,1]], i) for i in range(len(p1))], key=lambda x: x[0], reverse=True)
    p2i = sorted([(i2[p2[i,0], p2[i,1]], i) for i in range(len(p2))], key=lambda x: x[0], reverse=True)
    
    halflife1 = sum([p1i[i][0] > 0.67 * p1i[0][0] for i in range(len(p1i))]) 
    halflife2 = sum([p2i[i][0] > 0.67 * p2i[0][0] for i in range(len(p2i))])
    
    p1good = [p1[p1i[i][1]] for i in range(halflife1)]
    p2good = [p2[p2i[i][1]] for i in range(halflife2)]
    tree = kdtree.KDTree(p1good)
    
    q = tree.query(p2good)
    
    m2 = np.argmin(q[0])
    m1 = q[1][m2]
    d = q[0][m2]
    
    features = [np.mean(i1), np.mean(i2), np.var(i1), np.var(i2), halflife1, halflife2, d]
    
    for sigma in [0.7, 1 , 1.5 , 2.25 , 3.5, 5]:
        features.append(gaussian_filter(i1,sigma)[tuple(p1good[m1])])
        features.append(gaussian_filter(i2,sigma)[tuple(p2good[m2])])
        features.append(gaussian_laplace(i1,sigma)[tuple(p1good[m1])])
        features.append(gaussian_laplace(i2,sigma)[tuple(p2good[m2])])
    
    features.append(sobel(i1)[tuple(p1good[m1])])
    features.append(sobel(i2)[tuple(p2good[m2])])
    features.append(i1[tuple(p1good[m1])])
    features.append(i2[tuple(p2good[m2])])
    
    return [float(f) for f in features]

# static version id
getfeatures.version = '1'

def getfeatures2(im, halflifefactor = 0.5):
    '''
    new feature extraction, this time ignoring distance
    :param im: RGB-image
    :param halflifefactor: necessary brightness (factor of max brightness) for spot to be included in halflife
    :return: list of float-features
    '''
    
    
    # get channels 0,1 and detect local maxima
    i1 = im[:,:,0].astype(np.float)
    g1 = gaussian_filter(i1, 1)
    i2 = im[:,:,1].astype(np.float)
    g2 = gaussian_filter(i2, 1)
    p1 = peak_local_max(g1, min_distance=2)
    p2 = peak_local_max(g2, min_distance=2)
    
    # no! local maxima -> completely uniform img
    if len(p1) == 0 or len(p2) == 0:
        return None
    
    # get sorted (intensity, maxima_index) lists
    p1i = sorted([(i1[p1[i,0], p1[i,1]], i) for i in range(len(p1))], key=lambda x: x[0], reverse=True)
    p2i = sorted([(i2[p2[i,0], p2[i,1]], i) for i in range(len(p2))], key=lambda x: x[0], reverse=True)
    
    # get "halflife" := number of spots that have at least i(brightest spot) * halflifefactor 
    halflife1 = sum([p1i[i][0] > halflifefactor * p1i[0][0] for i in range(len(p1i))]) 
    halflife2 = sum([p2i[i][0] > halflifefactor * p2i[0][0] for i in range(len(p2i))])
    
    p1good = [p1[p1i[i][1]] for i in range(halflife1)]
    p2good = [p2[p2i[i][1]] for i in range(halflife2)]
    
    # "global" features: mean, var, halflifes
    features = [np.mean(i1), np.mean(i2), np.var(i1), np.var(i2), halflife1, halflife2]
    
    # "local" features at the brightst points in both channels
    for sigma in [0.7, 1 , 1.5 , 2.25 , 3.5, 5]:
        features.append(gaussian_filter(i1,sigma)[tuple(p1good[0])])
        features.append(gaussian_filter(i2,sigma)[tuple(p2good[0])])
        features.append(gaussian_laplace(i1,sigma)[tuple(p1good[0])])
        features.append(gaussian_laplace(i2,sigma)[tuple(p2good[0])])
    
    # some more local features without sigma dependence
    features.append(sobel(i1)[tuple(p1good[0])])
    features.append(sobel(i2)[tuple(p2good[0])])
    features.append(i1[tuple(p1good[0])])
    features.append(i2[tuple(p2good[0])])
    
    return [float(f) for f in features]

# static version id
getfeatures2.version = '2'
        

def predict_ml(img, sc, cls):
    feat = np.array(getfeatures(img)).reshape(1,-1)
    return ['good', 'bad', 'mediocre'][cls.predict(sc.transform(feat))]