"""
helper functions for distance normalization
"""

import os
import re
from multiprocessing import Pool
from functools import reduce
import json

from msr.msr_metadata import parse_msr, get_parameters_from_xml
from json_util import query_json
from image_io import read_image_stack
from fs_util import make_old_copy

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from scipy import ndimage as ndi

from skimage.color import label2rgb
from skimage.measure import regionprops

from pipeline import apply_multiple

def get_offseted_pos(msr_file, pos, series = 2):
    
    hd, ser = parse_msr(msr_file)
    series_ = ser[series][1]
    
    offsets = get_parameters_from_xml(series_, ['.doc/ExpControl/scan/range/x/off', '.doc/ExpControl/scan/range/y/off'])
    offsets = np.array(list(map(float, offsets)))
    size = get_parameters_from_xml(series_, ['.doc/ExpControl/scan/range/x/len', '.doc/ExpControl/scan/range/y/len'])
    size = np.array(list(map(float, size)))
    
    
    res = offsets - size / 2 +  np.array(pos, dtype=float)
    
    return res

def get_ov_pos(msr_file, pos, series = 0):
    hd, ser = parse_msr(msr_file)
    series_ = ser[series][1]
    size = get_parameters_from_xml(series_, ['.doc/ExpControl/scan/range/x/len', '.doc/ExpControl/scan/range/y/len'])
    size = np.array(list(map(float, size)))
    
    
    res = size / 2 +  np.array(pos, dtype=float)
    
    return res

def get_pixel_size(msr_file, series = 0):
    
    hd, ser = parse_msr(msr_file)
    series_ = ser[series][1]
    psz = get_parameters_from_xml(series_, ['.doc/ExpControl/scan/range/x/psz', '.doc/ExpControl/scan/range/y/psz'])
    psz = np.array(list(map(float, psz)))
    return psz
    
def to_pixel_index(pos, psz):
    return np.array(np.array(pos) / np.array(psz), dtype=int)

def read_current_results(acquisition_dir, analysis_info = 'analysis_info.json'):
    
    if not os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        return None
    
    current = query_json(os.path.join(acquisition_dir, analysis_info), '/analysis_current')
    if current is None:
        return None
    
    return pd.read_csv(os.path.join(acquisition_dir, current + '.csv'))
        

def get_name_until_level(name, level='field'):
    p_new = re.compile('(.*?_' + level +  '_[0-9]+).*?')
    p_old = re.compile('(.*?_' + level +  '[0-9]+).*?')
    oldpipeline = False
    m = p_new.match(name)
    if m is None:
        oldpipeline = True
        m = p_old.match(name)
        if m is None:
            return None
    return m.groups()[0], oldpipeline 


def dist_norm_field(pos, raw_detail, raw_ov, seg_ov, unit, oldpipeline):
    pos = np.array(pos, dtype = float) * unit
    pos = get_offseted_pos(raw_detail, pos, 2 if oldpipeline else 0)    
    pos = get_ov_pos(raw_ov, pos)    
    psz = get_pixel_size(raw_ov)    
    pos = to_pixel_index(pos, psz)
    
    seg_img = read_image_stack(seg_ov)
    # we need to reverse xy as imah index is yx
    
    outofbounds = False
    for i in range(len(pos)):
        if (list(reversed(pos))[i] >= seg_img.shape[i]):
            outofbounds = True
    
    if outofbounds:
        lab = 0
    else:
        lab = seg_img[tuple(reversed(pos))]
    
   
    
    if (lab != 0):
        rprops = regionprops(seg_img)[int(lab)-1]
    else:
        rprops = None
    
        # assuming x and y pixels are of the same size
    return {'equivalent_diameter': (rprops['equivalent_diameter'] * psz[0] / unit) if not (rprops is None) else None,
               'ov_pixel_size' : psz[0],
               'ov_xpos_pixel' : pos[0],
               'ov_ypos_pixel' : pos[1]}
    
    
    
    
def do_distance_normalization(acquisition_dir, analysis_info = 'analysis_info.json', outname = 'regionprops',
                              raw_dir = 'raw', segmentation = None, results = None, unit = 1e-6):
    """
    generate region properties for segmented nuclei in overview images at locations where spot pairs were found
    and save the as a .csv file
    :param acquisition_dir: the directory to process
    :param analysis_info: name of the JSON analysis ticket file
    :param outname: prefix for the file the results will be saved to
    :param raw_dir: directory containing raw files
    :param segmentation: name of the segmaentation to use, may be None, in which case we use the most recent
    :param results: name of the results (spot pair list) to use
        may be None, in which case we use the most recent
    :param unit: conversion factor from the distance unit Imspector uses (m) to the one Fiji use (um)
    :return: None    
    """

    # TODO: refactor this
    ad = acquisition_dir
    raw = raw_dir
    
    # read analysis info
    if not (os.path.exists(os.path.join(acquisition_dir, analysis_info))):
        info = dict()
    else:
        with open(os.path.join(acquisition_dir, analysis_info), 'r') as fd:
            info = json.load(fd)
    
    # get numerical suffix for results file
    p = re.compile(outname + '.*?(?:.*?([0-9]+).*?)+?\.csv')
    fs = next(os.walk(acquisition_dir))[2]
    max_idx = reduce(lambda x,y : max(x, int(p.match(y).groups()[0])) if not (p.match(y) is None) else x, fs, -1)
    
    
    # read analysis results
    if results is None:
        df = read_current_results(ad, analysis_info)
        results = query_json(os.path.join(acquisition_dir, analysis_info), '/analysis_current')
        if df is None:
            return None
    else:
        if os.path.exists(os.path.join(ad, results + '.csv')):
            df = pd.read_csv(os.path.join(ad, results + '.csv'))
        else:
            return None
    
    # query segmentation and check existence
    if segmentation is None:
        seg = query_json(os.path.join(ad, 'analysis_info.json'), 'current_segmentation')
        if seg is None:
            print('WARNING: no segmentation found for ' + acquisition_dir + ', skipping.')
            return None
    else:        
        seg = segmentation
        
    if not os.path.exists(os.path.join(ad, seg)):
        print('WARNING: requested segmentation not found for ' + acquisition_dir + ', skipping.')
        return None
    
    # construct out filename (without ending for the moment)
    outf = outname + '_' + results + '_' + seg + '_' + str(max_idx + 1)        
    
    tasks = []
    used_segs = []
    for row in df.iterrows():
        idx, data = row
        f = data.file
        name = f.rsplit(os.sep, 1)[-1]
        name_field, oldpipeline = get_name_until_level(name)

        raw_det = os.path.join(ad, raw, name )
        raw_ov = os.path.join(ad, raw, name_field + '.msr')
        seg_ov = os.path.join(ad, seg, name_field + '.msr.tif')    

        used_segs.append('.' + os.sep + os.path.relpath(seg_ov, ad))

        pos = np.array([(data.d01 + data.d02)/2. , (data.d11 + data.d12)/2.])

        tasks.append([pos, raw_det, raw_ov, seg_ov, unit, oldpipeline])

    # generate regionprop data multiprocessed
    pool = Pool()
    res = pool.starmap(dist_norm_field, tasks)
    pool.close()

    # build result table
    keys = set()
    for r in res:
        if not (r is None):
            keys |= r.keys()

    resd = dict()
    for k in keys:
        resd[k] = [r[k] if not (r is None) else np.nan for r in res]

    files = [f for f in df.file]


    resdf = pd.DataFrame()
    resdf['file'] = files
    resdf['segmented_ov'] = used_segs

    for k in keys:
        resdf[k] = resd[k]

    # save table
    resdf.to_csv(os.path.join(ad, outf + '.csv'), index=False)
    
    if not 'regionprops' in info.keys():
        info['regionprops'] = list()
    
    # update analysis log
    info_i = {
        'name' : outf,
        'segmentation' : seg,
        'results' : results
    }
    info['regionprops'].append(info_i)
    
    
    if os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        make_old_copy(os.path.join(acquisition_dir, analysis_info))
    with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
        json.dump(info, fd, indent = 2)
    
    print ('finished ' + acquisition_dir)


def main():
    file_path = '/Volumes/cooperation_data/TobiasRagoczy_StamLab/nanoFISH/15_visit_may_2017/K562_DLCR_HS1_HS4/K562_DLCR_HS1_HS4/raw/577808120f0c7294a56ea9fdc611129e_field1_sted1.msr'
    print(get_pixel_size(file_path, 2))

if __name__ == '__main__':
    main()