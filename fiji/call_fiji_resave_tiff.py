# @String tasks_file

import os
import sys
import inspect

sys.path.append(os.path.abspath(inspect.getfile(inspect.currentframe())).rsplit(os.sep, 1)[0])
import ij_util


def main():
    
    # set umask so that group has write access
    # FIXME: caused errors
    #os.umask(0o002)

    print(tasks_file)
    
    tasks = []
    
    fd = open(tasks_file, 'r')
    for l in fd.readlines():
        s = l.split(',')
        infile = s[0]
        outprefix = s[1]
        series = list(map(int, s[2:]))
        tasks.append((infile, outprefix, series))
    
    ij_util.resave_msrs(tasks, 'tiff')
        
    fd.close()

## __name__ is __builtin__ in newer fiji versions?
if __name__ in ['__main__', '__builtin__']:
    main()