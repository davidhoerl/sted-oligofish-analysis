import os
import time
from subprocess import run, PIPE, STDOUT

def call_resave_tiff(tasks_file, fiji_path='fiji', debug=False):
    
    script_path = os.path.join(os.path.abspath(__file__).rsplit(os.sep, 1)[0], 'call_fiji_resave_tiff.py')
    call = run((fiji_path + " --headless --ij2 --run " +  script_path + " tasks_file='" + tasks_file + "'").split(),
                stdout=PIPE, stderr=STDOUT, universal_newlines= True)
    if debug:
        print(time.asctime() + ' call: ' + (
            fiji_path + " --headless --ij2 --run " +  script_path + " tasks_file='" + tasks_file + "'"))
            
    return call.stdout

def call_resave_jpeg_sum(tasks_file, fiji_path='fiji', debug=False):
    
    script_path = os.path.join(os.path.abspath(__file__).rsplit(os.sep, 1)[0], 'call_fiji_resave_jpeg_sum.py')
    call = run((fiji_path + " --headless --ij2 --run " +  script_path + " tasks_file='" + tasks_file + "'").split(),
                stdout=PIPE, stderr=STDOUT, universal_newlines= True)

    if debug:
        print(time.asctime() + ' call: ' + (
        fiji_path + " --headless --ij2 --run " + script_path + " tasks_file='" + tasks_file + "'"))
            
    return call.stdout

def call_spot_analysis(tasks_file, fiji_path='fiji'):
    
    script_path = os.path.join(os.path.abspath(__file__).rsplit(os.sep, 1)[0], 'spot_analysis_fiji.py')
    call = run((fiji_path + " --headless --ij2 --run " +  script_path + " tasks_file='" + tasks_file + "'").split(),
                stdout=PIPE, stderr=STDOUT, universal_newlines= True)
            
    return call.stdout


def call_spot_extraction(tasks_file, fiji_path='fiji'):
    script_path = os.path.join(os.path.abspath(__file__).rsplit(os.sep, 1)[0], 'spot_extraction_fiji.py')
    call = run((fiji_path + " --headless --ij2 --run " + script_path + " tasks_file='" + tasks_file + "'").split(),
               stdout=PIPE, stderr=STDOUT, universal_newlines=True)

    return call.stdout
