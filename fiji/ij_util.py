from ij import IJ, ImagePlus
from loci.plugins import BF
from java.util.concurrent import Callable, Executors
from ij.plugin import ZProjector, RGBStackMerge
from ij.process import StackConverter
from net.imglib2.img.display.imagej import ImageJFunctions
from net.imglib2.algorithm.neighborhood import RectangleShape
from net.imglib2.view import Views

from java.lang import Runtime
from java.io import IOException
from java.lang import Exception


# manually import ImporterOptions, as the package name contains the "in" constant
ImporterOptions = __import__("loci.plugins.in.ImporterOptions", globals(), locals(), ['object'], -1)

class ResaveThread(Callable):
    def __init__(self, in_path, out_path, series, func):
        self.in_path = in_path
        self.out_path = out_path
        self.series = series
        self.func = func

    def call(self):
        self.func(self.in_path, self.out_path, self.series)
        print('finished file ' + self.in_path)

def importMSR(path):
    '''
    open MSR files
    returns array of stacks
    '''

    try:
        io = ImporterOptions()
        io.setId(path)
        io.setOpenAllSeries(True)
        imps = BF.openImagePlus(io)
    except IOException, e:
        IJ.log("ERROR while opening image file " + path)
        print(e.getStackTrace())
        return None

    return imps

def wrapFloat(imp):
    if imp.getNSlices() > 1:
        StackConverter(imp).convertToGray32()
    else:
        imp.setProcessor( imp.getProcessor().convertToFloatProcessor() )
    return ImageJFunctions.wrapFloat( imp )

def sumProjectZ(imp):
    calib = imp.getCalibration()
    zp = ZProjector(imp)
    zp.setMethod(ZProjector.SUM_METHOD)
    zp.doProjection()
    res = zp.getProjection()
    res.setCalibration(calib)
    return res

def resave_msr_as_tiff(in_path, out_path, series):

    imps = importMSR(in_path)

    if imps is None:
        print('WARNING: skipping ' + in_path + ' because of error.')
        return

    imps = [imps[i] for i in series]

    for i in range(len(imps)):
        IJ.saveAsTiff(imps[i], out_path + '_ch' + str(i) + '.tif')

def resave_msr_as_jpeg_sum(in_path, out_path, series):
    
    # FIXME:
    # crude nop on exception, handle cleanly!
    try:
        imps = importMSR(in_path)

        if imps is None:
            print('WARNING: skipping ' + in_path + ' because of error.')
            return

        imps = [imps[i] for i in series]

        impsProjected = []
        for impi in imps:
            zp = ZProjector(impi)
            zp.setMethod(ZProjector.SUM_METHOD)
            zp.doProjection()
            impsProjected.append(zp.getProjection())

        for impi in impsProjected:
            impi.resetDisplayRange()
            impi.updateImage()

        impComp = RGBStackMerge.mergeChannels(impsProjected, False)
        IJ.saveAs(impComp, 'JPEG', out_path + '.jpg')
    except Exception, e:
        print(e)

def doMedianND(img):
    """
    calculate n-dimensional median filter of img, return result as new Img
    # FIXME: this is super sloooow (why?), do not use at this point
    :param img: imglib2 Img input
    :return: a median filtered copy of img
    """
    med = img.copy()
    nbs = RectangleShape(1, False).neighborhoodsRandomAccessible(Views.extendZero(img))

    ra = nbs.randomAccess(img)
    cu = med.localizingCursor()

    while cu.hasNext():
        cu.fwd()
        ra.setPosition(cu)

        vals = []

        cuI = ra.get().cursor()
        while cuI.hasNext():
            cuI.fwd()
            vals.append(cuI.get().getRealDouble())

        vals = sorted(vals)

        if len(vals) % 2 == 0:
            res = (vals[len(vals) / 2] + vals[len(vals) / 2 - 1]) / 2
        else:
            res = vals[len(vals) / 2]

        cu.get().setReal(res)
    return med

def resave_msrs(tasks, task_type):

    MAX_CONCURRENT = Runtime.getRuntime().availableProcessors()
    service = Executors.newFixedThreadPool(MAX_CONCURRENT)

    if task_type == 'tiff':
        saver = resave_msr_as_tiff
    elif task_type == 'jpeg_sum':
        saver = resave_msr_as_jpeg_sum
    else:
        raise ValueError('wrong task type!')

    savers = list()
    for infile, outprefix, series in tasks:
        savers.append(ResaveThread(infile, outprefix, series, saver))

    futures = service.invokeAll(savers)
    for f in futures:
        f.get()

    print('-- ALL DONE --')
    service.shutdown()
