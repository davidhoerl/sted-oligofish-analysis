# @String tasks_file

import os
import sys
import math
import inspect
import pickle
import string
from csv import DictWriter

from ij import IJ, ImagePlus
from net.imglib2.algorithm.dog import DifferenceOfGaussian
from net.imglib2.algorithm.neighborhood import RectangleShape
from net.imglib2.view import Views
from net.imglib2.algorithm.localextrema import LocalExtrema, SubpixelLocalization
from net.imglib2.algorithm.localization import PeakFitter, EllipticGaussianOrtho, LevenbergMarquardtSolver, MLEllipticGaussianEstimator
from net.imglib2.util import Intervals
from net.imglib2 import RealPoint
from net.imglib2.util import Util
from net.imglib2.interpolation.randomaccess import NLinearInterpolatorFactory
from fiji.plugin.trackmate.detection import DetectionUtils

from java.lang import Runtime, Double
from java.util.concurrent import Callable, Executors

# manually import ImporterOptions, as the package name contains the "in" constant
ImporterOptions = __import__("loci.plugins.in.ImporterOptions", globals(), locals(), ['object'], -1)

sys.path.append(os.path.abspath(inspect.getfile(inspect.currentframe())).rsplit(os.sep, 1)[0])
import ij_util

def doDoG(img, expectedRadius, calib):
    service = Executors.newSingleThreadExecutor()
    sigma1 = expectedRadius / math.sqrt(img.numDimensions()) * 0.9
    sigma2 = expectedRadius / math.sqrt(img.numDimensions()) * 1.1
    sigmas = DifferenceOfGaussian.computeSigmas(0.5, 2, calib, sigma1, sigma2)
    dog = img.copy()
    DifferenceOfGaussian.DoG(sigmas[1], sigmas[0], Views.extendMirrorSingle(img), dog, service)
    service.shutdown()
    return dog

def getCalibration(imp):
    cal = imp.getCalibration()
    return [cal.pixelWidth, cal.pixelHeight, cal.pixelDepth]

def doMedianOps(img, ops):
    medfilt = img.copy()
    ops.filter().median(medfilt, img, RectangleShape(1, False))
    return medfilt

def doMedian(img):
    med = img.copy()
    DetectionUtils.applyMedianFilter(med)
    return med



def doRefineGaussian(img, spots, expectedSigma):

    fit = PeakFitter(Views.interval(Views.extendMirrorSingle(img), img), spots,
                     LevenbergMarquardtSolver(100, 1e-3, 1e-1),
                     EllipticGaussianOrtho(),
                     MLEllipticGaussianEstimator(expectedSigma))
    # "reasonable" number of threads
    fit.setNumThreads(8)
    fit.process()

    res_map = fit.getResult()

    res = [(RealPoint(res_map[s][0:img.numDimensions()]), res_map[s][img.numDimensions()+1:] , res_map[s][img.numDimensions()]) for s in spots]
    return res

def doRefineQuadratic(img, spots):

    refpeaks = SubpixelLocalization.refinePeaks(spots, Views.extendMirrorSingle(img), img, True, 10, True, .01, [True for _ in range(img.numDimensions())])
    res = [(r, None) for r in refpeaks]
    return res

def getMaxima(img, thresh):

    t = img.firstElement().createVariable()
    t.setReal(thresh)

    service = Executors.newSingleThreadExecutor()
    res = LocalExtrema.findLocalExtrema(Views.interval(Views.extendMirrorSingle(img), Intervals.expand(img,1)), LocalExtrema.MaximumCheck(t), service)
    service.shutdown()

    return res

def doDetection(imp1, imp2, expectedSigma, thresh, method=None, medfilt=True, zproject = False):

    if not (method is None) and not (method in ['gauss', 'quadratic']):
        raise ValueError("method must be None (no subpixel), 'gauss' or 'quadratic'")

    # z project if desired
    if zproject:
        imp1 = ij_util.sumProjectZ(imp1)
        imp2 = ij_util.sumProjectZ(imp2)

    #to imglib2
    img1 = ij_util.wrapFloat(imp1).copy()
    img2 = ij_util.wrapFloat(imp2).copy()

    # medfilt
    if medfilt:
        med1 = doMedian(img1)
        med2 = doMedian(img2)
    else:
        med1 = img1.copy()
        med2 = img2.copy()

    calib = getCalibration(imp1)
    expectedRadius = expectedSigma[0] * math.sqrt(2 * math.log(2))

    # dog
    dog1 = doDoG(med1, expectedRadius, calib)
    dog2 = doDoG(med2, expectedRadius, calib)

    # non-subpixel maxima
    max1 = getMaxima(dog1, thresh)
    max2 = getMaxima(dog2, thresh)


    # subpixel with desired method
    if method is None:
        res1 = [(RealPoint([m.getDoublePosition(i) * calib[i] for i in range(m.numDimensions())]), None) for m in max1]
        res2 = [(RealPoint([m.getDoublePosition(i) * calib[i] for i in range(m.numDimensions())]), None) for m in max2]
    elif method == 'gauss':
        rg1 = doRefineGaussian(img1, max1, [expectedSigma[i] / calib[i] for i in range(img1.numDimensions())])
        rg2 = doRefineGaussian(img2, max2, [expectedSigma[i] / calib[i] for i in range(img1.numDimensions())])

        res1 = [(RealPoint([m[0].getDoublePosition(i) * calib[i] for i in range(m[0].numDimensions())]),
                 [math.sqrt(1./si/2.) if si > 0 else 0. for si in m[1] ], m[2]) for m in rg1]
        res1 = [(m[0], [m[1][i] * calib[i] for i in range(img1.numDimensions())] + [m[2]]) for m in res1]
        res2 = [(RealPoint([m[0].getDoublePosition(i) * calib[i] for i in range(m[0].numDimensions())]),
                 [math.sqrt(1./si/2.) if si > 0 else 0. for si in m[1] ], m[2]) for m in rg2]
        res2 = [(m[0], [m[1][i] * calib[i] for i in range(img1.numDimensions())] + [m[2]]) for m in res2]

    elif method == 'quadratic':
        rq1 = doRefineQuadratic(dog1, max1)
        rq2 = doRefineQuadratic(dog2, max2)

        res1 = [(RealPoint([m[0].getDoublePosition(i) * calib[i] for i in range(m[0].numDimensions())]), None) for m in rq1]
        res2 = [(RealPoint([m[0].getDoublePosition(i) * calib[i] for i in range(m[0].numDimensions())]), None) for m in rq2]

    return (res1, res2, img1, img2)

def getBrightnessInterpolated(locs, img):
    # get rRA to interpolated source images
    rRAble = Views.interpolate(Views.extendMirrorSingle(img), NLinearInterpolatorFactory())
    rRA = rRAble.realRandomAccess()
    
    if (len(locs)<1):
        return []
    
    spotsAndInts = []
    for s in locs:
        rRA.setPosition(s[0])
        spotsAndInts.append((s, rRA.get().getRealDouble()))
    return spotsAndInts
    
def getBrightestPair(spots1, spots2, img1, img2):
    
    # abort if we have not found at least one spot in each channel
    if (len(spots1) < 1) or (len(spots2) < 1):
        return (None, None)
    
    spotsAndInts1 = getBrightnessInterpolated(spots1, img1)
    spotsAndInts2 = getBrightnessInterpolated(spots2, img2)
        
    spotsAndInts1 = sorted(spotsAndInts1, key=lambda x: x[1], reverse=True)
    spotsAndInts2 = sorted(spotsAndInts2, key=lambda x: x[1], reverse=True)
    
    return (spotsAndInts1[0], spotsAndInts2[0]) 

def doNN(spots1, spots2):

    # abort if we have not found at least one spot in each channel
    if (len(spots1) < 1) or (len(spots2) < 1):
        return (None, None)

    mindist = Double.MAX_VALUE
    res = (None, None)

    for s1 in spots1:
        for s2 in spots2:
            d = Util.distance(s1[0], s2[0])
            if d < mindist:
                mindist = d
                res = (s1, s2)

    return res


def make_res_line_single(spot, brightness, channel):
    res = dict()

    if spot is None:
        return None

    for i in range(spot[0].numDimensions()):
        res['d' + str(i)] = spot[0].getDoublePosition(i)

        if not (spot[1] is None):
            res['sigma' + str(i)] = spot[1][i]

    # peak of fitted Gaussian
    if not (spot[1] is None):
        res['peak_fit'] = spot[1][spot[0].numDimensions()]

    # brightness in raw image
    res['peak'] = brightness

    res['channel'] = channel
    return res

def make_res_line(spot1, spot2, brightness1, brightness2):
    res = dict()

    if (spot1 is None) or (spot2 is None):
        return None

    for i in range(spot1[0].numDimensions()):
        res['d' + str(i) + '1'] = spot1[0].getDoublePosition(i)
        res['d' + str(i) + '2'] = spot2[0].getDoublePosition(i)

        if not (spot1[1] is None):
            res['sigma' + str(i) + '1'] = spot1[1][i]
            res['sigma' + str(i) + '2'] = spot2[1][i]

    # peak of fitted Gaussian
    if not (spot1[1] is None):
        res['peak1_fit'] = spot1[1][spot1[0].numDimensions()]
        res['peak2_fit'] = spot2[1][spot2[0].numDimensions()]

    # brightness in raw image
    res['peak1'] = brightness1
    res['peak2'] = brightness2
    return res


class SpotExtractionCallable(Callable):
    ''' process a single file '''

    def __init__(self, tasks_info, i):
        '''
        ctor
        :param tasks_info: dict with list of files to process and parameters
        :param i: index of the file to process
        '''
        self.tasks_info_ = tasks_info
        self.i = i

    def call(self):
        try:
            tasks_info = self.tasks_info_
            path = tasks_info['files'][self.i]
            input_type = tasks_info['input_type']
            analysis_type = tasks_info['analysis_type']
            z_project = tasks_info['do_z_projection']

            print('IMAGE ' + str(self.i) + ': LOAD STARTED')
            if input_type == 'tiff':
                imp1 = IJ.openImage(path + '_ch0.tif')
                imp2 = IJ.openImage(path + '_ch1.tif')
            elif input_type == 'msr':
                series = tasks_info['series']
                imps = ij_util.importMSR(path)
                imps = [imps[j] for j in series]
                imp1 = imps[0]
                imp2 = imps[1]
            else:
                return None
            print('IMAGE ' + str(self.i) + ': LOAD FINISHED')

            thresh = tasks_info['thresh']
            fwhms = tasks_info['fwhms']
            expectedSigma = [fwhm / (2 * math.sqrt(2 * math.log(2))) for fwhm in fwhms]
            if z_project:
                expectedSigma = expectedSigma[0:2]

            print('IMAGE ' + str(self.i) + ': ANALYSIS STARTED')
            spots1, spots2, img1, img2 = doDetection(imp1, imp2, expectedSigma, thresh, analysis_type, True, z_project)

            # world coordiantes -> pixel coordinates
            calib = getCalibration(imp1)
            spots1 = [(RealPoint([m[0].getDoublePosition(i) / calib[i] for i in range(m[0].numDimensions())]), m[1]) for
                      m in spots1]
            spots2 = [(RealPoint([m[0].getDoublePosition(i) / calib[i] for i in range(m[0].numDimensions())]), m[1]) for
                      m in spots2]

            spots1 = getBrightnessInterpolated(spots1, img1)
            spots2 = getBrightnessInterpolated(spots2, img2)

            # pixel -> world
            spots1 = [((RealPoint([r[0].getDoublePosition(i) * calib[i] for i in range(r[0].numDimensions())]), r[1]), b) for r, b in spots1]
            spots2 = [((RealPoint([r[0].getDoublePosition(i) * calib[i] for i in range(r[0].numDimensions())]), r[1]), b) for r, b in spots2]

            res = [make_res_line_single(s,b,1) for s,b in spots1] + [make_res_line_single(s,b,2) for s,b in spots2]
            print('IMAGE ' + str(self.i) + ': ANALYSIS FINISHED')
            return res

        except Exception, e:
            print(e)
            return None


class SpotAnalysisThread(Callable):

    def __init__(self, tasks_info, i):
        self.tasks_info_ = tasks_info
        self.i = i

    def call(self):
        try:
            tasks_info = self.tasks_info_
            path = tasks_info['files'][self.i]
            input_type = tasks_info['input_type']
            analysis_type = tasks_info['analysis_type']
            z_project = tasks_info['do_z_projection']
            brightest = tasks_info['do_brightest'] if ('do_brightest' in tasks_info.keys()) else False

            print('IMAGE ' + str(self.i) + ': LOAD STARTED')
            if input_type == 'tiff':
                imp1 = IJ.openImage(path + '_ch0.tif')
                imp2 = IJ.openImage(path + '_ch1.tif')
            elif input_type == 'msr':
                series = tasks_info['series']
                imps = ij_util.importMSR(path)
                imps = [imps[j] for j in series]
                imp1 = imps[0]
                imp2 = imps[1]
            else:
                return None
            print('IMAGE ' + str(self.i) + ': LOAD FINISHED')

            thresh = tasks_info['thresh']
            fwhms = tasks_info['fwhms']
            expectedSigma = [fwhm / (2 * math.sqrt(2 * math.log(2))) for fwhm in fwhms]
            if z_project:
                expectedSigma = expectedSigma[0:2]

            print('IMAGE ' + str(self.i) + ': ANALYSIS STARTED')
            spots1, spots2, img1, img2 = doDetection(imp1, imp2, expectedSigma, thresh, analysis_type, True, z_project)
            
            # world coordiantes -> pixel coordinates
            calib = getCalibration(imp1)
            spots1 =[(RealPoint([m[0].getDoublePosition(i) / calib[i] for i in range(m[0].numDimensions())]), m[1]) for m in spots1]
            spots2 =[(RealPoint([m[0].getDoublePosition(i) / calib[i] for i in range(m[0].numDimensions())]), m[1]) for m in spots2]
            
            if brightest:
                print('doing brightest')
                ((r1, b1),(r2, b2)) = getBrightestPair(spots1, spots2, img1, img2)
            else:
                r1, r2 = doNN(spots1, spots2)
                b1 = getBrightnessInterpolated([r1], img1)[0][1]
                b2 = getBrightnessInterpolated([r2], img2)[0][1]
            
            # pixel -> world
            r1 = (RealPoint([r1[0].getDoublePosition(i) * calib[i] for i in range(r1[0].numDimensions())]), r1[1])
            r2 = (RealPoint([r2[0].getDoublePosition(i) * calib[i] for i in range(r2[0].numDimensions())]), r2[1])
                  
            res=  make_res_line(r1, r2, b1, b2)
            print('IMAGE ' + str(self.i) + ': ANALYSIS FINISHED')

            return res

        except Exception, e:
            print(e)
            return None






def main():

    # set umask so that group has write access
    #os.umask(0o002)
    
    if not os.path.exists(tasks_file):
        print('invalid tasks file')
        return

    fd = open(tasks_file, 'r')
    tasks_info = pickle.load(fd)
    fd.close()

    print(tasks_info)
    outpath = tasks_info['outpath']
    do_single_thread = tasks_info['do_single_thread']
    #do_single_thread = True


    nthreads = Runtime.getRuntime().availableProcessors()
    #nthreads = 2
    service = Executors.newFixedThreadPool(nthreads) if not do_single_thread else Executors.newSingleThreadExecutor()

    calls = []

    for i in range(len(tasks_info['files'])):
        calls.append(SpotAnalysisThread(tasks_info, i))

    futures = []

    for c in calls:
        try:
            futures.append(service.submit(c))
        except Exception, e:
            print(e)

    res = []
    for f in futures:
        try:
            ri = f.get()
            res.append(ri)
        except Exception, e:
            print(e)
            res.append(None)

    print('GOT ALL FUTURES')

    service.shutdown()

    print('THREAD POOL SHUTDOWN')

    header = set()

    for r in res:
        if r is None:
            continue
        header = header | set(r.keys())

    header = ['file'] + sorted(list(header))

    outdir = outpath.rsplit(os.sep, 1)[0]

    outfd = open(outpath, 'w')
    writer = DictWriter(outfd, fieldnames=header)
    writer.writerow(dict(zip(header, header)))

    for i in range(len(res)):

        if res[i] is None:
            continue

        rt = res[i]
        rt['file'] = string.replace(tasks_info['files'][i], outdir, '.')

        writer.writerow(rt)

    outfd.close()



if __name__ in ('__main__', '__builtin__'):
    main()
