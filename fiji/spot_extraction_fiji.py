# @String tasks_file

import sys
import os
import inspect
import string
import pickle
from csv import DictWriter

from java.lang import Runtime, Double
from java.util.concurrent import Callable, Executors

sys.path.append(os.path.abspath(inspect.getfile(inspect.currentframe())).rsplit(os.sep, 1)[0])
import ij_util
import spot_analysis_fiji as span

def main():
    
    # set umask so that group has write access
    # FIXME: caused errors
    #os.umask(0o002)

    if not os.path.exists(tasks_file):
        print('invalid tasks file')
        return

    fd = open(tasks_file, 'r')
    tasks_info = pickle.load(fd)
    fd.close()

    print(tasks_info)
    outpath = tasks_info['outpath']
    do_single_thread = tasks_info['do_single_thread']
    #do_single_thread = True


    nthreads = Runtime.getRuntime().availableProcessors()
    #nthreads = 2
    service = Executors.newFixedThreadPool(nthreads) if not do_single_thread else Executors.newSingleThreadExecutor()

    calls = []

    for i in range(len(tasks_info['files'])):
        calls.append(span.SpotExtractionCallable(tasks_info, i))

    futures = []

    for c in calls:
        try:
            futures.append(service.submit(c))
        except Exception, e:
            print(e)

    res = []
    for f in futures:
        try:
            ri = f.get()
            res.append(ri)
        except Exception, e:
            print(e)
            res.append(None)

    print('GOT ALL FUTURES')

    service.shutdown()

    print('THREAD POOL SHUTDOWN')

    header = set()

    for r in res:
        if r is None:
            continue
        for ri in r:
            if ri is None:
                continue
            header = header | set(ri.keys())

    header = ['file'] + sorted(list(header))

    outdir = outpath.rsplit(os.sep, 1)[0]

    outfd = open(outpath, 'w')
    writer = DictWriter(outfd, fieldnames=header)
    writer.writerow(dict(zip(header, header)))

    for i in range(len(res)):

        if res[i] is None:
            continue

        for rt in res[i]:
            if rt is None:
                continue

            rt['file'] = string.replace(tasks_info['files'][i], outdir, '.')
            writer.writerow(rt)

    outfd.close()

if __name__ in ('__main__', '__builtin__'):
    main()