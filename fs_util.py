import os
import re
import shutil

def make_old_copy(file, suffix='old', save_max=5):
    d = file.rsplit(os.sep, 1)[0]
    f = file.rsplit(os.sep, 1)[-1]
    p = re.compile(re.escape(f + suffix) + '([0-9]+)')
    idxes = []
    for fi in next(os.walk(d))[2]:
        m = p.match(fi)
        if not m is None:
            idxes.append(int(m.groups()[0]))
    
    if len(idxes) >= save_max:
        os.remove(file + suffix + str(max(idxes)))
    
    idxes_remaining = sorted(idxes, reverse=True)[1:] if len(idxes) >= save_max else sorted(idxes, reverse=True)
    
    for i in idxes_remaining:
        shutil.move(file + suffix + str(i), file + suffix + str(i + 1))
        
    shutil.copy(file, file + suffix + '0')


def mkdir_if_necessary(path):
    if not os.path.exists(path):
        os.makedirs(path)
        
def symlink_if_necessary(dest, src, force_overwrite = False):
    if not os.path.exists(dest):
        os.symlink(src, dest)
    elif force_overwrite and os.path.exists(dest):
        os.remove(dest)
        os.symlink(src, dest)