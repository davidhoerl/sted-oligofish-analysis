## Image QC using HDF5 datasets

Code for image QC (good/bad sorting) using the whole-dataset HDF5 files. ```hdf_sorting.py``` allows for quick manual sorting of images and ```train_h5_classifier.ipynb``` contains code for training a new classifier from those sortings.