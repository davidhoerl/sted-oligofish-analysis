import re
import os
from functools import reduce
import tkinter
from tkinter import messagebox
import json
from collections import defaultdict
from operator import add

import numpy as np
from scipy import ndimage as ndi
import h5py as h5
import matplotlib
matplotlib.use("Qt5Agg")
from matplotlib import pyplot as plt


### SETTINGS

# folder containing the raw files
raw_folder = '/Users/katharina/Desktop/Desktop/2D-Data_for_Sorting/A204'
# file to save the sorting to
out_file = '/Users/katharina/Desktop/sortingA204.json'

# at which level to sort
upper_level = 'sted'

# plotting color range
scale_red = (0, 30)
scale_green = (0, 30)
scale = (scale_red, scale_green)

### DEFS

def get_data(acquisition, configurations=(0,), channels=(0, 1)):
    res = []
    for c in configurations:
        for ch in channels:
            dta = np.squeeze(np.array(acquisition[str(c)][str(ch)]))
            res.append(dta)
    return tuple(res)


def levels_to_str(levels):
    return '_'.join(reduce(add, levels))


def plot_rgb(data1, data2, scales=((0, 255), (0,255)), project_fun=np.sum, smooth=False):

    f, axs = plt.subplots(3, 2)
    (ax00, ax01, ax10, ax11, ax20, ax21) = axs.ravel()

    if smooth:
        data1 = ndi.gaussian_filter(data1, 1.0)
        data2 = ndi.gaussian_filter(data2, 1.0)

    xy_rgb = np.dstack(
        [project_fun(data1, axis=0), project_fun(data2, axis=0), np.zeros_like(project_fun(data1, axis=0))])
    xy_rgb[:,:,0] = np.clip(xy_rgb[:,:,0], scales[0][0], scales[0][1])
    xy_rgb[:,:,1] = np.clip(xy_rgb[:,:,1], scales[1][0], scales[1][1])
    xy_rgb[:,:,0] = np.interp(xy_rgb[:,:,0], scales[0], (0, 255))
    xy_rgb[:,:,1] = np.interp(xy_rgb[:,:,1], scales[1], (0, 255))
    xy_rgb = xy_rgb.astype(np.uint8)
    ax00.imshow(xy_rgb)
    ax00.set_title('XY')

    xz_rgb = np.dstack(
        [project_fun(data1, axis=1), project_fun(data2, axis=1), np.zeros_like(project_fun(data1, axis=1))])
    xz_rgb[:,:,0] = np.clip(xz_rgb[:,:,0], scales[0][0], scales[0][1])
    xz_rgb[:,:,1] = np.clip(xz_rgb[:,:,1], scales[1][0], scales[1][1])
    xz_rgb[:,:,0] = np.interp(xz_rgb[:,:,0], scales[0], (0, 255))
    xz_rgb[:,:,1] = np.interp(xz_rgb[:,:,1], scales[1], (0, 255))
    xz_rgb = xz_rgb.astype(np.uint8)
    ax01.imshow(xz_rgb)
    ax01.set_title('XZ')

    '''
    yz_rgb = np.dstack(
        [project_fun(data1, axis=2), project_fun(data2, axis=2), np.zeros_like(project_fun(data1, axis=2))])
    yz_rgb = np.clip(yz_rgb, scales[0], scales[1]).astype(np.int)
    yz_rgb = np.interp(yz_rgb, scales, (0, 255)).astype(np.int)
    '''

    ax10.imshow(xy_rgb[:,:,0], cmap='gray')
    ax10.set_title('Channel 1')

    ax11.imshow(xz_rgb[:,:,0], cmap='gray')
    ax11.set_title('Channel 1')

    ax20.imshow(xy_rgb[:,:,1], cmap='gray')
    ax20.set_title('Channel 2')

    ax21.imshow(xz_rgb[:,:,1], cmap='gray')
    ax21.set_title('Channel 2')


### MAIN

p = re.compile('_?(.+?)_?([0-9]+)')
quality = defaultdict(dict)

if os.path.exists(out_file):
    with open(out_file, 'r') as jfd:
        quality.update(json.load(jfd))

root = tkinter.Tk()
root.withdraw()

try:
    for file in [os.path.join(raw_folder, f) for f in os.listdir(raw_folder) if f.endswith('.h5')]:
        with h5.File(file, 'r') as fd:
            filename = file.rsplit(os.sep)[-1]
            ex = fd['experiment']
            for k in ex.keys():
                levels = p.findall(k)
                is_level = reduce(lambda o, n: o or n[0] == upper_level, levels, False)
                lvlstr = levels_to_str(levels)

                if lvlstr in quality[filename]:
                    continue

                if is_level:

                    data_c0, data_c1 = get_data(ex[k])

                    plot_rgb(data_c0, data_c1, scales=scale, project_fun=np.max)

                    plt.draw()
                    plt.show(block=False)

                    decision = messagebox.askyesnocancel("Good or bad?")
                    plt.close('all')

                    if decision is None:
                        raise InterruptedError()

                    quality[filename][lvlstr] = decision

except InterruptedError:
    print('Interrupted by user.')
except Exception as e:
    print(e)

# save file
finally:
    with open(out_file, 'w+') as jfd:
        json.dump(quality, jfd, indent=1)

root.destroy()
