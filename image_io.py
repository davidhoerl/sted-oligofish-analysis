import numpy as np
from PIL import Image

def read_image_stack(path, correct16bit=False, get_info = False):
    """
    :param path: path to .tif file
    :return: stack of layers
    """
    image_file = Image.open(path)
    image_list = list()
    n = 0
    while True:
        (w, h) = image_file.size
        image_list.append(np.array(image_file.getdata()).reshape(h, w))
        n += 1
        try:
            image_file.seek(n)
        except:
            break

    if len(image_list) == 1:
        res = image_list[0]
    else:
        res = np.dstack(image_list)
    
    image_file.seek(0)
    info = image_file.info
    
    if correct16bit:
        res = res - np.iinfo(np.int16).max - 1

    image_file.close()
    
    if get_info:
        return res, info
    else:
        return res
    
def save_2d_tiff(img, info, file):
    out_img = Image.fromarray(np.array(img, dtype=np.uint8))
    out_img.info = info
    out_img.save(file)