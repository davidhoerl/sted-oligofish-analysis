import os
import json

def recursive_dict_query(d, query, sep='/'):
    query = query.strip(sep)
    query = query.split(sep)
    try:
        while(len(query) >= 1):
            if len(query) == 1:
                return d[query[0]]
            else:
                d = d[query[0]]
                query = query[1:]            
    except KeyError:
        return None
    
def query_json(filepath, query, sep = '/'):
    if not os.path.exists(filepath):
        return None
    with open(filepath, 'r') as fd:
        d = json.load(fd)
    return recursive_dict_query(d, query, sep)