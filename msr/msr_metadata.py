from xml.etree import ElementTree
import struct
from pprint import pprint
import warnings

def read_fmt(fd, fmt):
    size = struct.calcsize(fmt)
    return struct.unpack(fmt, fd.read(size))[0]


def read_header(fd):

    FILE_MAGIC_STRING = b'OMAS_BF\n'
    MAGIC_NUMBER = 0xFFFF

    fmt_to_read = ['I', "Q"]

    magic_string_ = fd.read(len(FILE_MAGIC_STRING))
    magic_nr_ = read_fmt(fd, 'H')

    if not ((magic_string_ == FILE_MAGIC_STRING) and (magic_nr_ == MAGIC_NUMBER)):
        return None

    version = read_fmt(fd, 'I')
    first_stack_offset = read_fmt(fd, 'Q')

    # skip 4 bytes
    fd.read(4);

    # read metadata offset
    metadata_offset = None
    if version > 1:
        metadata_offset = read_fmt(fd, 'Q')

    # skip 5 bytes
    fd.read(5)

    old_version = try_parse_root_header_desc(fd)

    if not old_version:
        fd.read(5)


    # desclen
    desc_len = read_fmt(fd, "H")

    desc = fd.read(desc_len if old_version else (desc_len * 2))

    return (first_stack_offset, desc.decode('ascii' if old_version else 'utf_16_le'), metadata_offset)


def try_parse_root_header_desc(fd):
    '''
    check if the file under investigation was written by imspector 0.11
    this function is to be called with fd at the point where 0.11 files will have a short desc_len
    followed by the ascii description (starting with <root)

    fd will remain at its original position
    '''
    init_pos = fd.tell()
    # skip desclen
    fd.seek(2, 1)
    # read desc start
    bts = fd.read(5)
    # jump back to where we were
    fd.seek(init_pos)
    # did we arrive at <root ?
    return bts == b'<root'


def read_stack(fd, pos):
    STACK_MAGIC_STRING = b"OMAS_BF_STACK\n"
    MAGIC_NUMBER = 0xFFFF
    STACK_VERSION = 5
    MAXIMAL_NUMBER_OF_DIMENSIONS = 15

    fd.seek(pos)

    stack_magic_sting = fd.read(len(STACK_MAGIC_STRING))
    stack_magic_nr = read_fmt(fd, 'H')
    stack_version = read_fmt(fd, 'i')


    if not (stack_magic_sting == STACK_MAGIC_STRING and stack_magic_nr == MAGIC_NUMBER):
        return None
            
    if not (stack_version <= STACK_VERSION):
        warnings.warn('Reading MSR stack version {}, code was only tested with version {}'.format(stack_version, STACK_VERSION))

    number_of_dimensions = read_fmt(fd, "i")

    dims = []
    for i in range(MAXIMAL_NUMBER_OF_DIMENSIONS):
        d = read_fmt(fd, "i")
        dims.append(d if i < number_of_dimensions else 1)

    lengths = []
    for i in range(MAXIMAL_NUMBER_OF_DIMENSIONS):
        d = read_fmt(fd, "d")
        lengths.append(d if i < number_of_dimensions else 0.0)

    offsets = []
    for i in range(MAXIMAL_NUMBER_OF_DIMENSIONS):
        d = read_fmt(fd, "d")
        offsets.append(d if i < number_of_dimensions else 0.0)


    pType = read_fmt(fd, "i")
    compr = read_fmt(fd, "i")

    fd.read(4)

    lengthOfName = read_fmt(fd, "i")
    lengthOfDescription = read_fmt(fd, "i")

    fd.read(8)

    lengthOfData = read_fmt(fd, "q")
    nextPos =  read_fmt(fd, "q")

    name = fd.read(lengthOfName)

    # TODO: this doesn't seem to do anything
    description = fd.read(lengthOfDescription)

    fd.seek(lengthOfData, 1)
    footerStart = fd.tell()

    footerSize = read_fmt(fd, "I")
    fd.read(4*MAXIMAL_NUMBER_OF_DIMENSIONS)
    fd.read(4*MAXIMAL_NUMBER_OF_DIMENSIONS)
    firstMetaLen = read_fmt(fd, "I")

    fd.seek(footerStart)
    fd.seek(footerSize, 1)

    for i in range(number_of_dimensions):
        nameLen = read_fmt(fd, "I")
        fd.seek(nameLen,1)

    # skip first xml
    fd.seek(firstMetaLen, 1)


    # FIXME: hacky! seek until we find '<root>'
    accum = b''
    while not accum.endswith(b'<root'):
        accum += fd.read(1)

    fd.seek(-9, 1)

    sndMetaLen = read_fmt(fd, "I")
    StackMeta = fd.read(sndMetaLen)

    return name, nextPos, StackMeta.decode('utf-8')


def get_parameters_from_xml(xml, keys):
    res = []
    myET = ElementTree.fromstring(xml)

    for k in keys:
        res.append(str(myET.find(k).text))

    return res


def parse_msr(path):
    fd = open(path, "rb")
    next_offset, header_desc, meta_offset = read_header(fd)
    #print(header_desc)
    res = []

    offset = next_offset
    while offset != 0:
        name, offset, StackMeta = read_stack(fd, offset)
        res.append((name, StackMeta))

    fd.close()
    return header_desc, res


def printParameters(params):
    print(params[0])
    myET = ElementTree.fromstring(params[2])

    print ("dwelltime:\t" + str(myET.find(".doc/ExpControl/scan/dwelltime").text))
    print ("line accu:\t" + str(myET.find(".doc/ExpControl/scan/range/line_accu").text))
    print ("STED power:\t" + str(myET.find(".doc/STED775/power").text))
    print ("3D power:\t" + str(myET.find(".doc/ExpControl/three_d/modules/item/mrs/position/value/calibrated").text))
    print ("pinhole size:\t" + str(myET.find(".doc/Pinhole/pinhole_size").text))

    chans = str(myET.find(".doc/ExpControl/gating/linesteps/chans_enabled").text)
    chans = chans.split(" ")
    chansIdx = []
    for i in range(len(chans)):
        if chans[i] == b"1":
            chansIdx.append(i)
    print(chansIdx)

    laser_ena = str(myET.find(".doc/ExpControl/gating/linesteps/laser_enabled").text)
    laser_ena = laser_ena.split(" ")
    laserEnaIdx = []
    for i in range(len(laser_ena)):
        if laser_ena[i] == b"1":
            laserEnaIdx.append(i)
    print(laserEnaIdx)

    chans_on = str(myET.find(".doc/ExpControl/gating/linesteps/chans_on").text)
    laser_on = str(myET.find(".doc/ExpControl/gating/linesteps/laser_on").text)

    # print detector for all enabled channels
    chanDets = myET.findall(".doc/ExpControl/scan/detsel/detsel/item")
    for i in chansIdx:
        print ( "channel " + str(i+1) + " detector:\t" + str(chanDets[i].text))

    # print power for all enabled lasers
    laserPwrs = myET.findall(".doc/ExpControl/lasers/power_calibrated/item/value/calibrated")
    for i in laserEnaIdx:
        print ( "laser " + str(i+1) + " power:\t" + str(laserPwrs[i].text))



def main():

    file_path = '/Volumes/cooperation_data/TobiasRagoczy_StamLab/nanoFISH/15_visit_may_2017/K562_DLCR_HS1_HS4/K562_DLCR_HS1_HS4/raw/577808120f0c7294a56ea9fdc611129e_field1_sted1.msr'

    hd, stacks = parse_msr(file_path)

    for name, meta in stacks:
        print(meta)



if __name__ == '__main__':
    main()
