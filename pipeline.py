"""
under-the-hood functions for nanofish data handling/analysis
"""

import os
import re
import json
import pickle

from shutil import copy
from tempfile import NamedTemporaryFile
from multiprocessing import Pool, cpu_count
from functools import reduce
from operator import add
from itertools import cycle
from collections import defaultdict

import numpy as np
from skimage.io import imread
from scipy import ndimage as ndi
from skimage import morphology, measure, feature, filters
from skimage.color import label2rgb
from matplotlib import pyplot as plt
from skimage import segmentation

import pandas as pd
import h5py as h5

from fiji import fiji_ext
from fs_util import mkdir_if_necessary, symlink_if_necessary, make_old_copy
from json_util import query_json, recursive_dict_query
from classification import getfeatures, getfeatures2, getfeatures_h5
from image_io import read_image_stack, save_2d_tiff

ANALYSIS_VERSION = 3

def apply_filter(names, filter_):
    """
    select only names that conform to a given filter
    :param names: a list of strings, typicalliy file/directory names
    :param filter_: a (string, list)-tuple, the string indicating which type of filter to apply and the list containing items to select for
            type can be 'numerical', in which case only names starting with a numerical prefix that is in the list will be kept
            or 'name', in which case only names in the filter_-list will be kept
    :return: names conforming to the filter, a list of strings
    """
    
    if not (type(filter_) is tuple):
        raise TypeError("filter must be a tuple consisting of a string and a list")    
    
    if filter_[0] == 'numerical':
        p = re.compile('([0-9]+).*?')
        return [n for n in names if (not (p.match(n) is None)) and int(p.match(n).groups()[0]) in filter_[1]]
    if filter_[0] == 'name':
        return [n for n in names if n in filter_[1]]
    else:
        raise ValueError("filter type must be 'numerical' or 'name'")


def apply_multiple(root_dir, fun, slide_group_filter = None, slide_filter = None, acquisition_filter = None,
                                    acquisition_check = None, dry_run = True):
    """
    go through the nanofish data and apply a function to all acquisitions conforming to various filters
    :param root_dir: the root directory of all the nanofish data
    :param fun: function that is applied to every acquisition directory not filtered out,
        should take a single argument, the absolute path of acquisition dir
    :param slide_group_filter: (optional) filter that is applied to all slide group directory names, see :func:`apply_filter`
    :param slide_filter: (optional) filter that is applied to all slide directory names, see :func:`apply_filter`
    :param acquisition_filter: (optional) filter that is applied to all acquisition directory names, see :func:`apply_filter`
    :param acquisition_check: (optional) function that takes the absolute path of an acquisition and returns a boolean,
        indicating whether we should process that acquisition or not
    :param dry_run: whether to actually do the task or just print what we are about to do
    :return: list of pairs, the first element of each being the processed dir and the second the output of fun
    """


    res = []
    
    _, slide_groups, _ = next(os.walk(root_dir))
    
    # apply slide group filters
    if not (slide_group_filter is None):
        slide_groups = apply_filter(slide_groups, slide_group_filter)
        
    for slide_group in slide_groups:
        
        _, slides, _ = next(os.walk(os.path.join(root_dir, slide_group)))
    
        # apply slide filters
        if not (slide_filter is None):
            slides = apply_filter(slides, slide_filter)
                            
        for slide in slides:
            
            _, acquisitions, _ = next(os.walk(os.path.join(root_dir, slide_group, slide)))
    
            # apply acquisition filters
            if not (acquisition_filter is None):
                acquisitions = apply_filter(acquisitions, acquisition_filter)
                                      
            
            for acquisition in acquisitions:
                
                if not (acquisition_check is None):
                    if not acquisition_check(os.path.join(root_dir, slide_group, slide, acquisition)):
                        continue
                
                if dry_run:
                    print('will process acquisition dir: ' + os.path.join(root_dir, slide_group, slide, acquisition))
                else:
                    res_i = fun(os.path.join(root_dir, slide_group, slide, acquisition))
                    res.append((os.path.join(root_dir, slide_group, slide, acquisition), res_i))
    
    return res


def get_features_file_(file, featurefun=getfeatures):
    """
    read an image file :param file: and apply the function :param featurefun: to get image features for classification
    :return: np-array
    """
    img = imread(file)
    feats = featurefun(img)
    return feats


def get_features_and_classes(acquisition_dir, sorting = None, proj_dir = 'jpeg_sum_projection',
       analysis_info='analysis_info.json', featurefun = getfeatures, classfun = lambda d : 0 if d in ['good'] else 1):
    """
    get features and classes from already sorted images
    :param acquisition_dir: dir to process
    :param sorting: dir containing sorted files, if this is None, we pick the current sorting from analyisis info JSON
    :param proj_dir: directory containing JPEG sum projections
    :param analysis_info: analysis info JSON file
    :param featurefun: function to generate features from a file (filename -> np-array)
    :param classfun: function to generate class index from class directory name
    :return:
    """

    if sorting is None:
        # query latest 'manual' sorting
        sorting_info = next(reversed([s for s in query_json(os.path.join(acquisition_dir, analysis_info), '/sorting') if s['type'] == 'manual']), None)
        if sorting_info is None:
            print ('sorting not found for ' + acquisition_dir)
            return ([], [])
        sorting = sorting_info['name']
    else:
        sorting_info = next(reversed([s for s in query_json(os.path.join(acquisition_dir, analysis_info), '/sorting') if s['name'] == sorting]), None)
    
    if sorting_info is None:
        print ('sorting not found for ' + acquisition_dir)
        return ([], [])
    if sorting_info['type'] != 'manual':
        print ("selected sorting is not of type 'manual' for " + acquisition_dir)
        return ([], [])
    
    
    p = re.compile('.*?(?:_(.+?)([0-9]+))+.*?')

    resclasses = []    
    tasks = []
    
    for d in next(os.walk(os.path.join(acquisition_dir, sorting)))[1]:
        
        classi = classfun(d)        
        for f in next(os.walk(os.path.join(acquisition_dir, sorting, d)))[2]:
            
            m = p.match(f)
            if m is None:
                continue
            level = m.groups()[0]
            
            proj_file = os.path.join(acquisition_dir, proj_dir, level, f + '.jpg')
            if not os.path.exists(proj_file):
                continue
            
            
            tasks.append((proj_file, featurefun))
            resclasses.append(classi)
    
    service = Pool()
    resfeats = service.starmap(get_features_file_, tasks)
    service.close()
    
    return (resfeats, resclasses)
    

def create_hierarchical_raw_links(acquisition_dir, raw_dir = 'raw', out_dir = 'raw_hierarchical',
                                  dry_run = False, force_overwrite = False):
    """
    create hierarchically sorted links to raw data
    :param acquisition_dir: base dir of the acquisition
    :param raw_dir: subdir containing raw .msr files
    :param out_dir: subdir to create resulting links in
    :param dry_run: whether to actually do the work or just dry run
    :param force_overwrite: whether to overwrite hierarchical links if they exist already
    :return: None
    """
    
    # set umask so that group has write access
    os.umask(0o002)

    # new pipeline inserts an underscore between level name and index, old pipeline did not
    p_new = re.compile('.*?(?:_(.+?)_([0-9]+))+.*?')
    p_old = re.compile('.*?(?:_(.+?)_?([0-9]+))+.*?')

    d_out = os.path.join(acquisition_dir, out_dir)
    
    if dry_run:
        print('mkdir ' + d_out)
    else:
        mkdir_if_necessary(d_out)
    
    d_raw, _, fs = next(os.walk(os.path.join(acquisition_dir, raw_dir)))
    
    levels = []
    
    for f in fs:

        # we first check if the filename conforms to new format
        m = p_new.match(f)
        if m is None:
            # fallback to old format
            m = p_old.match(f)
        # no match -> ignore
        if m is None:
            continue
          
        if not m.groups()[0] in levels:
            if dry_run:
                print('mkdir ' + os.path.join(d_out, m.groups()[0] ))
            else:
                mkdir_if_necessary(os.path.join(d_out, m.groups()[0] ))
            levels.append(m.groups()[0])
        
        dir_dest = os.path.join(d_out, m.groups()[0])
        link_src = os.path.join(d_raw, f)
        link_src = os.path.relpath(link_src, dir_dest)
        
        if dry_run:
            print('ln ' + os.path.join(dir_dest, f ) + ' -> ' + link_src)
        else:
            symlink_if_necessary(os.path.join(dir_dest, f ), link_src, force_overwrite)

            
def resave_acquisition(acquisition_dir, resave_type, hierarchy_dir='raw_hierarchical', out_dir=None, dry_run = False,
                              series_to_use = {'field' : [0,1], 'sted': [2,3]}, force_overwrite = False,
                            fiji_path='fiji', print_output = False, debug=False, verbose=False):
    """
    resave the images of an acquisition to TIFF or JPEG
    :param acquisition_dir: acquisition base dir
    :param resave_type: which resaving to do. may be 'tiff' or 'jpeg_sum_projection'
    :param hierarchy_dir: dir containing hierarchically sorted raw files
    :param out_dir: (optional) dir to save results to, if None, will use resave_type as name
    :param dry_run: whether to do a dry run
    :param series_to_use: dict indication which series in .msr files to use for each hierarchy level
    :param force_overwrite: whether to overwrite existing output
    :param fiji_path: path to the FIJI executable used for analysis
    :param print_output: whether to print FIJI output
    :return: None
    """
    
    # set umask so that group has write access
    os.umask(0o002)

    if not (resave_type in ['tiff', 'jpeg_sum_projection']):
        raise ValueError("supported resave types are 'tiff' and 'jpeg_sum_projection'")
    
    if out_dir is None:
        out_dir = resave_type
    
    d_out = os.path.join(acquisition_dir, out_dir)
    if dry_run:
        print('mkdir ' + d_out)
    else:
        mkdir_if_necessary(d_out)
    
    levels = []
    
    infiles = []
    outfiles = []
    series = []
    
    
    for d, ds, fs in os.walk(os.path.join(acquisition_dir, hierarchy_dir)):
        for f in fs:
            infile = os.path.join(d, f)
            outfile = infile.replace(os.path.join(acquisition_dir, hierarchy_dir), os.path.join(acquisition_dir, out_dir))
            
            if infile.split(os.sep)[-2] not in levels:
                if dry_run:
                    print('mkdir ' + os.path.join(d_out, infile.split(os.sep)[-2] ))
                else:
                    mkdir_if_necessary(os.path.join(d_out, infile.split(os.sep)[-2] ))
                levels.append(infile.split(os.sep)[-2] )
            
            if dry_run:
                i = 0
                if resave_type == 'tiff':
                    for se in series_to_use[infile.split(os.sep)[-2]]:
                        print(infile + ' -> ' + outfile + '_ch' + str(i) + '.tif')
                        i = i + 1
                elif resave_type == 'jpeg_sum_projection':
                    print(infile + ' -> ' + outfile + '.jpg')
            else:
                infiles.append(infile)
                outfiles.append(outfile)
                series.append(series_to_use[infile.split(os.sep)[-2]])
                
    
    if not dry_run:
        try:
            ntf = NamedTemporaryFile(suffix='.tasks', delete=False)
            for i in range(len(infiles)):
                if not force_overwrite:
                    all_present = True
                    if resave_type == 'tiff':
                        for j in range(len(series[i])):
                            all_present = all_present and os.path.exists(outfiles[i] + '_ch' + str(j) + '.tif')
                    elif resave_type == 'jpeg_sum_projection':
                        all_present = os.path.exists(outfiles[i] + '.jpg')
                    if all_present:
                        if verbose:
                            print('skipping ' + infiles[i] + ', was already resaved')
                        continue
                
                ntf.writelines([bytes(','.join([infiles[i]] + [outfiles[i]] + list(map(str, series[i]))) + '\n', 'utf-8')])
        
            ntf.close()

            if resave_type == 'tiff':
                res = fiji_ext.call_resave_tiff(ntf.name, fiji_path=fiji_path, debug=debug)
            elif resave_type == 'jpeg_sum_projection':
                res = fiji_ext.call_resave_jpeg_sum(ntf.name, fiji_path=fiji_path, debug=debug)
            if print_output:
                print(res)
            
            
        finally:
            if not debug:
                os.remove(ntf.name)
        
        print('finished ' + acquisition_dir)



def sort_outer(acquisition_dir, sorting_fun, analysis_info='analysis_info.json',
               sorted_dir = 'sorted', dry_run=True):    
    """
    framework function for sorting images
    :param acquisition_dir: base dir of acquisition
    :param sorting_fun: function to do the actual sorting, should take two parameters, acquisition base dir and subdir for sorted images
    :param analysis_info: analysis info JSON file name
    :param sorted_dir: base name for sorting results, this will receive a numerical suffix
    :param dry_run: whether to do a dry run
    :return: None
    """

    # set umask so that group has write access
    os.umask(0o002)
    
    if os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        with open(os.path.join(acquisition_dir, analysis_info)) as fd:
            info = json.load(fd)
    else:
        info = dict()
        
    if not 'sorting' in info.keys():
        info['sorting'] = list()
        
    p = re.compile('.*?([0-9]+)')
    idx_sorting = 0 if len(info['sorting']) == 0 else max(map(int, [p.match(s['name']).groups()[0] for s in info['sorting']])) + 1
    
    info['sorting_current'] = sorted_dir + str(idx_sorting)
        
    sorting_info = sorting_fun(acquisition_dir, sorted_dir + str(idx_sorting))
    
    info['sorting'].append(sorting_info)
    
    if dry_run:
        print(json.dumps(info, indent = 2))
    else:
        if os.path.exists(os.path.join(acquisition_dir, analysis_info)):
            make_old_copy(os.path.join(acquisition_dir, analysis_info))
        with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
            json.dump(info, fd, indent=2)


def get_prefix_and_levels(filename):
    p = re.compile('(.*?)_(.+?)_([0-9]+).*?')
    gs = p.findall(filename)
    return gs[0][0], reduce(add, [g[1:] for g in gs])

# feature calculation for a batch of indices in a single hdf5 file
def getfeatures_multi(h5file, levels):
     res = []
     with h5.File(h5file, 'r') as fd:
         for level in levels:
             res.append(getfeatures_h5(fd, level))
     return res

def get_features_and_classes2(raw_dirs, jsons):

    nproc = cpu_count()
    c = cycle(range(nproc))
    h5files = {}
    for raw_dir, json_i in zip(raw_dirs, jsons):

        with open(json_i, 'r') as fd:
            sorting_i = json.load(fd)

        for k,v in sorting_i.items():

            h5file = os.path.join(raw_dir, k)
            if not h5file in h5files:
                h5files[h5file] = []
                for _ in range(nproc):
                    h5files[h5file].append([])

            for k1, v1 in v.items():
                h5files[h5file][next(c)].append((k1, v1))


    p = Pool()
    feats_pool = []
    classes_pool = []
    for k, v in h5files.items():

        # get features
        task = []
        classes = []
        for vi in v:
            task.append((k, [vii[0].split('_') for vii in vi]))
            classes.extend([0 if vii[1] else 1 for vii in vi])

        res = p.starmap(getfeatures_multi, task)

        # mask bad indices (features == None)
        feats = reduce(add, res)
        n_imgs = len(feats)
        bad_idxes = {i for (i, f) in enumerate(feats) if f is None}
        feats = [f for f in feats if f is not None]
        classes = [c for (i, c) in enumerate(classes) if not i in bad_idxes]

        feats_pool.extend(feats)
        classes_pool.extend(classes)

    return feats_pool, classes_pool



def sort_sklearn_inner_hdf5(acquisition_dir, sorted_dir, classifier, scaler, classes = ['good', 'bad'],
                       raw_dir = 'raw', hierarchy_level = 'sted',
                       dry_run = True, featurefun=getfeatures_h5, badclass=1):

    # set umask so that group has write access
    os.umask(0o002)
    
    # unpickle scaler and classifier
    with open(scaler, 'rb') as fd:
        sc = pickle.load(fd)

    with open(classifier, 'rb') as fd:
        cls = pickle.load(fd)

    # make output directories
    out_dir = os.path.join(acquisition_dir, sorted_dir)
    if dry_run:
        print('mkdir ' + out_dir)
        for cl in classes:
            print('mkdir ' + os.path.join(out_dir, cl))
    else:
        mkdir_if_necessary(out_dir)
        for cl in classes:
            mkdir_if_necessary(os.path.join(out_dir, cl))

    # get all raw files
    _, _, raw_files = next(os.walk(os.path.join(acquisition_dir, raw_dir)))
    # exclude .h5 files
    raw_files = [rf for rf in raw_files if not rf.endswith('.h5')]
    # only use files of desired level
    raw_files = [rf for rf in raw_files if get_prefix_and_levels(rf)[1][-2] == hierarchy_level]

    # for each hdf5 file:
    # split feature generation into nCores work lists
    nproc = cpu_count()
    c = cycle(range(nproc))
    h5files = {}
    for rf in raw_files:
        prefix, levels = get_prefix_and_levels(rf)
        h5file = os.path.join(acquisition_dir, raw_dir, prefix + '.h5')
        if not h5file in h5files:
            h5files[h5file] = []
            for _ in range(nproc):
                h5files[h5file].append([])
        h5files[h5file][next(c)].append(levels)


    # multiprocessed feat generation and classification
    p = Pool()
    # result JSON
    json_sort = defaultdict(dict)
    for k, v in h5files.items():

        # get features
        task = []
        for vi in v:
            task.append((k, vi))
        res = p.starmap(getfeatures_multi, task)

        # mask bad indices (features == None)
        feats = reduce(add, res)
        n_imgs = len(feats)
        bad_idxes = {i for (i, f) in enumerate(feats) if f is None}
        feats = [f for f in feats if f is not None]

        # classify
        cls_res = list(cls.predict(sc.transform(feats)).astype(np.int))
        cls_it = iter(cls_res)

        # re-introduce bad indices
        cls_res_l = []
        for i in range(n_imgs):
            if i in bad_idxes:
                cls_res_l.append(badclass)
            else:
                cls_res_l.append(next(cls_it))

        # populate sorting JSON
        for idx, clazz in zip(reduce(add, v), cls_res_l):
            json_sort[k]['_'.join(idx)] = bool(clazz != badclass)

        # make raw_files_i
        raw_files_i = []
        for idx in reduce(add, v):
            file_ii = '_'.join([k.replace('.h5', '')] + list(idx)) + '.msr'
            file_ii = file_ii.rsplit(os.sep,1)[-1]
            raw_files_i.append( file_ii )

        # link sorted files
        for f, cls_res in zip(raw_files_i, cls_res_l):
            dir_dest = os.path.join(out_dir, classes[int(cls_res)])
            link_src = os.path.join(acquisition_dir, raw_dir, f)
            link_src = os.path.relpath(link_src, dir_dest)

            if dry_run:
                print('ln ' + os.path.join(dir_dest, f) + ' -> ' + link_src)
            else:
                symlink_if_necessary(os.path.join(dir_dest, f), link_src)

    # get classifier path
    pre = os.path.commonprefix([acquisition_dir, classifier])
    rel_cls = os.path.relpath(classifier, pre)

    # save json
    json_file = out_dir.rstrip(os.sep) + '.json'
    if not dry_run:
        with open(json_file, 'w') as fd:
            json.dump(json_sort, fd, indent=1)

    res = dict()
    res['name'] = sorted_dir
    res['sorting_file'] = json_file.rsplit(os.sep, 1)[-1]
    res['type'] = 'auto, scikit-learn (HDF5)'
    res['classifier'] = rel_cls
    res['feature-version'] = str(featurefun.version)
    return res

def sort_sklearn_inner(acquisition_dir, sorted_dir, classifier, scaler, classes = ['good', 'bad'],
                       raw_dir = 'raw', sum_proj_dir = 'jpeg_sum_projection', hierarchy_level = 'sted',
                       dry_run = True, featurefun=getfeatures):
    """
    actual sorting using a scikit-learn classifier working on image features
    :param acquisition_dir: base dir of acquisition
    :param sorted_dir: dir to save results to
    :param classifier: sklearn classifier, should predict a numerical index
    :param scaler: sklearn scaler
    :param classes: names of the classes (with the indexes predicted by classifier)
    :param raw_dir: directory of raw files
    :param sum_proj_dir: directory of JPEG sum projection files
    :param hierarchy_level: which hierarchy level to work in
    :param dry_run: whether to do a dry run
    :return: dict containing info about the sorting parameters
    """
    
    # set umask so that group has write access
    os.umask(0o002)
    
    with open(scaler, 'rb') as fd:
        sc = pickle.load(fd)

    with open(classifier, 'rb') as fd:
        cls = pickle.load(fd)
        
    out_dir = os.path.join(acquisition_dir, sorted_dir )
    
    if dry_run:
        print('mkdir ' + out_dir)
        for cl in classes:
            print('mkdir ' + os.path.join(out_dir, cl))
    else:
        mkdir_if_necessary(out_dir)
        for cl in classes:
            mkdir_if_necessary(os.path.join(out_dir, cl))
        
    _, _, raw_files = next(os.walk(os.path.join(acquisition_dir, raw_dir)))
    
    proj_dir_ = os.path.join(acquisition_dir, sum_proj_dir, hierarchy_level)
    
    # return immediately if we have no detailed images (this should be caught earlier bz an acquisition_check)
    if not os.path.exists(proj_dir_):
        print('WARNING: no images at hierarchy level {} found for {}'.format(*[hierarchy_level, acquisition_dir]))
        res = dict()
        res['name'] = sorted_dir
        res['type'] = 'auto, scikit-learn'
        return res
    
    _, _, sum_proj_files = next(os.walk(proj_dir_))
    
    raw_files = [f for f in raw_files if f + '.jpg' in sum_proj_files]
    
        
    pool = Pool()
    
    tasks = []    
    for f in raw_files:
        tasks.append([proj_dir_, f, cls, sc, featurefun])
        
    cls_res_l = pool.starmap(get_classification_result, tasks)
    pool.close()
    
    
    for f, cls_res in zip(raw_files, cls_res_l):    
        dir_dest = os.path.join(out_dir, classes[int(cls_res)])
        link_src = os.path.join(acquisition_dir, raw_dir, f)
        link_src = os.path.relpath(link_src, dir_dest)
        
        if dry_run:
            print('ln ' + os.path.join(dir_dest, f ) + ' -> ' + link_src)
        else:
            symlink_if_necessary(os.path.join(dir_dest, f ), link_src)
    
    pre = os.path.commonprefix([acquisition_dir, classifier])
    rel_cls = os.path.relpath(classifier, pre)             
    
    res = dict()
    res['name'] = sorted_dir
    res['type'] = 'auto, scikit-learn'
    res['classifier'] = rel_cls
    res['feature-version'] = str(featurefun.version)
    return res



def get_classification_result(proj_dir, f, classifier, scaler, featurefun=getfeatures, badclass=1):
        im = imread(os.path.join(proj_dir, f + '.jpg'))
        feat_list = featurefun(im)
        if feat_list is None:
            return badclass
        feat = np.array(feat_list).reshape([1,-1])
        cls_res = int(classifier.predict(scaler.transform(feat)))
        return cls_res

    
def sort_copy_old_inner(acquisition_dir, sorted_dir, old_dir, sorting_type = 'manual',
                  raw_dir = 'raw', dry_run = True):
    """
    actual sorting functions for copying sorting from old data dir to new format
    :param acquisition_dir: acquisition base dir
    :param sorted_dir: dir to save results to
    :param old_dir: 'old-style' dir containing raw files (and JPEGs) in subfolders corresponding to classes
    :param sorting_type: type of sorting, for documentation in analysis info
    :param raw_dir: dir with raw files
    :param dry_run: whether to do a dry run
    :return: dict containing info about sorting parameters
    """

    # set umask so that group has write access
    os.umask(0o002)
    
    out_dir = os.path.join(acquisition_dir, sorted_dir )
    
    if dry_run:
        print('mkdir ' + out_dir)
        print('mkdir ' + os.path.join(out_dir, 'good'))
        print('mkdir ' + os.path.join(out_dir, 'bad'))
    else:
        mkdir_if_necessary(out_dir)
        mkdir_if_necessary(os.path.join(out_dir, 'good'))
        mkdir_if_necessary(os.path.join(out_dir, 'bad'))
    
    _, _, raw_files = next(os.walk(os.path.join(acquisition_dir, raw_dir)))    
    _, old_dirs, _ = next(os.walk(old_dir))
    
    for od in old_dirs:
        if not od in ['good', 'bad', 'mediocre']:
            continue
        for f in next(os.walk(os.path.join(old_dir, od)))[2]:
            if f in raw_files:
                dir_dest = os.path.join(out_dir, 'good') if od == 'good' else os.path.join(out_dir, 'bad')
                link_src = os.path.join(acquisition_dir, raw_dir, f)
                link_src = os.path.relpath(link_src, dir_dest)
                
                if dry_run:
                    print('ln ' + os.path.join(dir_dest, f ) + ' -> ' + link_src)
                else:
                    symlink_if_necessary(os.path.join(dir_dest, f ), link_src)
                    
    
    res = dict()
    res['name'] = sorted_dir
    res['type'] = sorting_type
    return res
    
    
def sort_copy_old(acquisition_dir, old_dir, raw_dir='raw', sorting_type = 'manual', dry_run=True,
                 analysis_info='analysis_info.json', sorted_dir = 'sorted'):
    """

    :param acquisition_dir:
    :param old_dir:
    :param raw_dir:
    :param sorting_type:
    :param dry_run:
    :param analysis_info:
    :param sorted_dir:
    :return:
    """
    
    fun = lambda ad, sd : sort_copy_old_inner(ad, sd, old_dir=old_dir, sorting_type=sorting_type,
                                              raw_dir=raw_dir, dry_run=dry_run)
    sort_outer(acquisition_dir, fun, analysis_info=analysis_info, sorted_dir=sorted_dir, dry_run=dry_run)
    
    

def sort_sklearn(acquisition_dir, classifier, scaler, analysis_info='analysis_info.json',
               sorted_dir = 'sorted', classes = ['good', 'bad'], raw_dir = 'raw',
                 sum_proj_dir = 'jpeg_sum_projection', hierarchy_level = 'sted', dry_run = True,
                featurefun=getfeatures):
    """
    wrapper function for sorting an acquisition using a sklearn classifier
    :param acquisition_dir: acquisition base dir
    :param classifier: file containing pickled classifier
    :param scaler:  file containing pickled scaler
    :param analysis_info: analysis info JSON file name
    :param sorted_dir: prefix of output dir
    :param classes: classes to sort into (indices should correspond to numerical predictions of classifier)
    :param raw_dir: raw data subdir
    :param sum_proj_dir: sum projection JPEG subdir
    :param hierarchy_level: which hierarchy level to work on
    :param dry_run: whether to do a dry run
    :return: None
    """
    
    fun = lambda ad, sd : sort_sklearn_inner(ad, sd, classifier, scaler, classes=classes, raw_dir=raw_dir,
                                             sum_proj_dir=sum_proj_dir, hierarchy_level=hierarchy_level,
                                         dry_run=dry_run, featurefun=featurefun)
    
    sort_outer(acquisition_dir, fun, analysis_info=analysis_info, sorted_dir=sorted_dir, dry_run=dry_run)


def do_analysis(acquisition_dir, analysis_type = 'gauss', out_file = 'results', sorted_dir = None,
                class_ = 'good', dry_run = False, series = [2,3], analysis_info = 'analysis_info.json',
                threshold_dog = 1.0, expected_fwhms = [.08, .08, .5], tiff_dir = None,
                fiji_path = 'fiji', print_output=False, make_tasks_copy = False, do_single_thread = False,
                visualization = None, z_project = False, detect_brightest = True):
    """
    do spot pair analysis for an acquisition
    :param acquisition_dir: base dir of the acquisition
    :param analysis_type: which type of subpixel localization. may be None, 'gauss' or 'quadratic'
    :param out_file: name of the file to save to. will receive a numerical postfic and .csv ending
    :param sorted_dir: directory containing sorted images, if None, we pick the most current sorting from analysis info JSON
    :param class_: the class in the sorting to use
    :param dry_run: whether to do a dry run
    :param series: which series (for .msr input) to use
    :param analysis_info: analysis info JSON file name
    :param threshold_dog: threshold for maxima detection via DoG
    :param expected_fwhms: expected FWHMs of spots (in image coordinates)
    :param tiff_dir: (optional) directory containing TIFF-resaved images. if this is not None, the files herein will be used as input
    :param fiji_path: path to the FIJI executable used for analysis
    :param print_output: wether to print FIJI output
    :param make_tasks_copy: wether to save a copy of the tasks file, will print the filename
    :param do_single_thread: wether to do the analysis single-threaded, for debug
    :param visualization: wether to immediately visualize results. may be None 'show' or 'save'
    :param z_project: wether to sum project the images in z before spot detection
    :param detect_brightest: flag to detect only the brightest spots in each channel, not nearest pair
    :return: None
    """
    
    # set umask so that group has write access
    os.umask(0o002)

    if ( not (sorted_dir is None) ) and ( not (os.path.exists(os.path.join(acquisition_dir, sorted_dir, class_)))):
        print('WARNING: sorted dir ' + os.path.join(acquisition_dir, sorted_dir, class_) + ' does not exist')
        return
    
    with open(os.path.join(acquisition_dir, analysis_info)) as fd:
        info = json.load(fd)
    
    if sorted_dir is None:
        sorted_dir = recursive_dict_query(info, '/sorting_current')
    
    indir_sorted = os.path.join(acquisition_dir, sorted_dir, class_)
    
    if not 'analyses' in info.keys():
        info['analyses'] = list()
        
    p = re.compile('.*?([0-9]+)')
    idx_analysis = 0 if len(info['analyses']) == 0 else max(map(int, [p.match(s['name']).groups()[0] for s in info['analyses']])) + 1
    
    fins = [f for f in next(os.walk(indir_sorted))[2]]
    
    if tiff_dir is None:
        fin_abs = [os.path.join(indir_sorted, f) for f in fins]
    else:
        fin_abs = []
        for dr, sd, fs in os.walk(os.path.join(acquisition_dir, tiff_dir)):
            for f in fins:
                if (f + '_ch0.tif' in fs) and (f + '_ch1.tif' in fs):
                    fin_abs.append(os.path.join(dr, f))
    
    
    tasks = dict()
    tasks['input_type'] = 'msr' if tiff_dir is None else 'tiff'
    tasks['series'] = series
    tasks['analysis_type'] = analysis_type
    tasks['files'] = fin_abs
    tasks['thresh'] = threshold_dog
    tasks['outpath'] = os.path.join(acquisition_dir, out_file + str(idx_analysis) + '.csv')
    tasks['fwhms'] = expected_fwhms
    tasks['do_single_thread'] = do_single_thread
    tasks['do_z_projection'] = z_project
    tasks['do_brightest'] = detect_brightest

    
        
    if make_tasks_copy:
        ntf = NamedTemporaryFile(suffix='.tasks', delete=False)
        pickle.dump(tasks, ntf, protocol=0)
        ntf.close()
        print('tasks copy saved to: ' + ntf.name)
    
    if dry_run:
        print('will process the following tasks: ')
        print(json.dumps(tasks, indent= 2))
    else:
        try:
            ntf = NamedTemporaryFile(suffix='.tasks', delete=False)
            pickle.dump(tasks, ntf, protocol=0)
            ntf.close()
    
            res = fiji_ext.call_spot_analysis(ntf.name, fiji_path)
            
            if print_output:
                print(res)
                
            print('finished ' + acquisition_dir)
    
        finally:
            #pass
            os.remove(ntf.name)
            
    info_i = {'name': out_file + str(idx_analysis),
                'type': analysis_type,
                 'sorting' : sorted_dir,
                 'tiff_input' : tiff_dir,
                 'threshold_dog' : threshold_dog,
                 'expected_fwhms' : expected_fwhms,
                 'z_projected' : z_project,
                 'brightest_pair' : detect_brightest,
                 'analysis_version' : ANALYSIS_VERSION}
    
    info['analyses'].append(info_i)
    info['analysis_current'] = out_file + str(idx_analysis)
    
    if dry_run:
        print(json.dumps(info, indent=2))
   
    else:
        if os.path.exists(os.path.join(acquisition_dir, analysis_info)):
            make_old_copy(os.path.join(acquisition_dir, analysis_info))
        with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
            json.dump(info, fd, indent=2)
            
    if not (visualization is None):
        if visualization == 'show':
            make_result_visualization(acquisition_dir, analysis_info, results=out_file + str(idx_analysis), save_results = False,
                              sample = None, out_dir = 'results_visualization', tiff_dir = 'tiff')
        elif visualization == 'save':
            make_result_visualization(acquisition_dir, analysis_info, results=out_file + str(idx_analysis), save_results = True,
                              sample = None, out_dir = 'results_visualization', tiff_dir = 'tiff')


def do_spot_extraction(acquisition_dir, analysis_type = 'gauss', sorted_dir = None,
                class_ = 'good', dry_run = False, series = [2,3], analysis_info = 'analysis_info.json',
                threshold_dog = 1.0, expected_fwhms = [.08, .08, .5], tiff_dir = None,
                fiji_path = 'fiji', print_output=False, make_tasks_copy = False, do_single_thread = False,
                z_project = False):
    """
    do spot pair analysis for an acquisition
    :param acquisition_dir: base dir of the acquisition
    :param analysis_type: which type of subpixel localization. may be None, 'gauss' or 'quadratic'
    :param out_file: name of the file to save to. will receive a numerical postfic and .csv ending
    :param sorted_dir: directory containing sorted images, if None, we pick the most current sorting from analysis info JSON
    :param class_: the class in the sorting to use
    :param dry_run: whether to do a dry run
    :param series: which series (for .msr input) to use
    :param analysis_info: analysis info JSON file name
    :param threshold_dog: threshold for maxima detection via DoG
    :param expected_fwhms: expected FWHMs of spots (in image coordinates)
    :param tiff_dir: (optional) directory containing TIFF-resaved images. if this is not None, the files herein will be used as input
    :param fiji_path: path to the FIJI executable used for analysis
    :param print_output: wether to print FIJI output
    :param make_tasks_copy: wether to save a copy of the tasks file, will print the filename
    :param do_single_thread: wether to do the analysis single-threaded, for debug
    :param z_project: wether to sum project the images in z before spot detection
    :return: data_frame
    """

    # set umask so that group has write access
    os.umask(0o002)
    
    if (not (sorted_dir is None)) and (not (os.path.exists(os.path.join(acquisition_dir, sorted_dir, class_)))):
        print('WARNING: sorted dir ' + os.path.join(acquisition_dir, sorted_dir, class_) + ' does not exist')
        return

    with open(os.path.join(acquisition_dir, analysis_info)) as fd:
        info = json.load(fd)

    if sorted_dir is None:
        sorted_dir = recursive_dict_query(info, '/sorting_current')

    indir_sorted = os.path.join(acquisition_dir, sorted_dir, class_)

    if not 'analyses' in info.keys():
        info['analyses'] = list()

    fins = [f for f in next(os.walk(indir_sorted))[2]]

    if tiff_dir is None:
        fin_abs = [os.path.join(indir_sorted, f) for f in fins]
    else:
        fin_abs = []
        for dr, sd, fs in os.walk(os.path.join(acquisition_dir, tiff_dir)):
            for f in fins:
                if (f + '_ch0.tif' in fs) and (f + '_ch1.tif' in fs):
                    fin_abs.append(os.path.join(dr, f))

    tasks = dict()
    tasks['input_type'] = 'msr' if tiff_dir is None else 'tiff'
    tasks['series'] = series
    tasks['analysis_type'] = analysis_type
    tasks['files'] = fin_abs
    tasks['thresh'] = threshold_dog
    tasks['fwhms'] = expected_fwhms
    tasks['do_single_thread'] = do_single_thread
    tasks['do_z_projection'] = z_project

    if make_tasks_copy:
        ntf = NamedTemporaryFile(suffix='.tasks', delete=False)
        pickle.dump(tasks, ntf, protocol=0)
        ntf.close()
        print('tasks copy saved to: ' + ntf.name)

    if dry_run:
        print('will process the following tasks: ')
        print(json.dumps(tasks, indent=2))
    else:
        try:
            ntf = NamedTemporaryFile(suffix='.tasks', delete=False)
            outfile = NamedTemporaryFile(suffix='.csv', delete=False)
            tasks['outpath'] = outfile.name
            pickle.dump(tasks, ntf, protocol=0)
            ntf.close()

            res = fiji_ext.call_spot_extraction(ntf.name, fiji_path)

            if print_output:
                print(res)

            print('finished ' + acquisition_dir)

        finally:
            # pass
            res = pd.read_csv(outfile.name)
            os.remove(ntf.name)
            os.remove(outfile.name)

        if res is not None:
            df = res
            # add slide_group, slide and acquisition id (determined from the path if acquisition_dir)
            s = acquisition_dir.rsplit(os.sep, 3)
            ac = s[-1]
            sl = s[-2]
            sg = s[-3]
            df['slide_group_id'] = sg
            df['slide_id'] = sl
            df['acquisition_id'] = ac
            return df


def flatten_acquisition_info(d, prefix='', d_sep='/'):
    """
    flatten a json-like dict into a single-level dict
    :param d: the dict to flatten
    :param prefix: prefix for keys in output dict, we need this for recursive calls
    :param d_sep: how to separate hierarchy levels in d
    :return:
    """
    if type(d) is dict:
        res = dict()
        for k, v in d.items():
            for ki, vi in flatten_acquisition_info(v, prefix + d_sep + str(k)).items():
                res[ki] = vi

        return res
    elif type(d) is list:
        res = dict()
        for i in range(len(d)):
            for ki, vi in flatten_acquisition_info(d[i], prefix + d_sep + str(i)).items():
                res[ki] = vi
        return res

    else:
        if d is None:
            d = np.nan
        return {prefix : d}


def query_most_recent_analysis(info, analysis_type = None, analysis_name = None, z_projected = None, analysis_version = None):
    """
    get the most recent analysis, optionally filtered by name, type, z_projection or version
    :param info:
    :param analysis_type:
    :param analysis_name:
    :param z_projected:
    :param analysis_version:
    :return:
    """
    check = lambda a : ( True if (analysis_type is None) else (a['type'] == (analysis_type if analysis_type != 'none' else None)) and
                       ( True if (analysis_name is None) else (a['name'] == analysis_name)) and
                       ( True if (z_projected is None) else (a.get('z_projected', False) == z_projected)) and # default zproj is False
                       ( True if (analysis_version is None) else (a.get('analysis_version', 1) == analysis_version ))) # default version is 1
    
    if info.get('analyses', None) is None:
        return None
    
    try:
        res = next(a for a in reversed(info['analyses']) if check(a))
    except (StopIteration):
        return None
    return res['name']


def get_analysis(acquisition_dir, analysis_name = None, analysis_type = None, z_projected = False,
                 analysis_info = 'analysis_info.json', acquisition_info = 'acquisition_info.json',
                 add_regionprops = True, analysis_version = None ):
    """
    get analysis results for an acquisition as pandas dataframe
    :param acquisition_dir: the directory to process
    :param analysis_name: which analysis to process (may be None, in this case the most recent analysis (optionally restricted by type)
    :param analysis_type: which type the analysis should be (note that None indicates no preference, if we want analyses with no subpixel loc, use the string 'none;)
    :param analysis_info: filename of json analysis info
    :param acquisition_info: filname of json acquisition info
    :param add_regionprops: add most recent regeinprops for selected analysis if available
    :return: pandas dataframe for selected analysis of selected acquisition
    """

    if not os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        print ('skipping ' + acquisition_dir + ', no analysis info found')
        return None

    if not os.path.exists(os.path.join(acquisition_dir, acquisition_info)):
        print('skipping ' + acquisition_dir + ', no acquisition info found')
        return None

    # # we care neither about name, nor about type -> get most current
    # if (analysis_name is None) and (analysis_type is None):
    #     analysis_name = query_json(os.path.join(acquisition_dir, analysis_info), '/analysis_current')
    #     if analysis_name is None:
    #         print('skipping ' + acquisition_dir + ', no analysis found')
    #         return None
    #
    # # we only care about type -> get most recent of type
    # elif (analysis_name is None) and not (analysis_type is None):
    #     try:
    #         analysis_type_ = None if analysis_type == 'none' else analysis_type
    #         analysis_name = next((a for a in reversed(query_json(os.path.join(acquisition_dir, analysis_info), '/analyses')) if a['type'] ==  analysis_type_))['name']
    #     except (StopIteration, TypeError):
    #         print ('skipping ' + acquisition_dir + ', no analysis of type ' + analysis_type + ' found')
    #         return None
    #
    # # we care about name, try to get analysis with given name
    # elif not (analysis_name is None) and (analysis_type is None):
    #     try:
    #         analysis_name =next((a for a in reversed(query_json(os.path.join(acquisition_dir, analysis_info), '/analyses')) if a['name'] ==  analysis_name))['name']
    #     except (StopIteration, TypeError):
    #         print ('skipping ' + acquisition_dir + ', analysis ' + analysis_name + ' not found')
    #         return None
    # # we care about both name and and type
    # else:
    #     try:
    #         analysis_name = next((a for a in reversed(query_json(os.path.join(acquisition_dir, analysis_info), '/analyses'))
    #                               if (a['type'] ==  analysis_type) and (a['name'] ==  analysis_name)))['name']
    #     except (StopIteration, TypeError):
    #         print ('skipping ' + acquisition_dir + ', no analysis of type ' + analysis_type + ' with name ' + analysis_name + ' found')
    #         return None

    with open(os.path.join(acquisition_dir, analysis_info), 'r') as fd:
        info = json.load(fd)

    analysis_name = query_most_recent_analysis(info, analysis_type, analysis_name, z_projected, analysis_version)
    if analysis_name is None:
        print('skipping ' + acquisition_dir + ', no analysis with requested properties found')
        return None

    # try to find regionprops
    found_rprops = False
    if add_regionprops:
        rprops = query_json(os.path.join(acquisition_dir, analysis_info), '/regionprops')
        if (rprops is None) or len(rprops) == 0:
            print('WARNING: no regionprops for ' + acquisition_dir + ' found.')
            pass
        else:
            rprops_for_results = [r for r in rprops if r['results'] == analysis_name]
            if len(rprops_for_results) == 0:
                print('WARNING: no regionprops for ' + acquisition_dir + ' found.')
                pass
            else:
                found_rprops = True
                rprops_to_use = next(reversed(rprops_for_results))['name']
        
    df = pd.read_csv(os.path.join(acquisition_dir, analysis_name + '.csv'))

    with open(os.path.join(acquisition_dir, acquisition_info), 'r') as fd:
        acquisition_info_ = json.load(fd)

    for k, v in flatten_acquisition_info(acquisition_info_).items():
        df[k] = v
        
    if found_rprops:
        rprops_df = pd.read_csv(os.path.join(acquisition_dir, rprops_to_use + '.csv'))        
        df = df.merge(rprops_df, on = ['file'])
    
    # we have an empty results DataFrame, just return None
    if df.shape[0] < 1:
        return None
    
    # add slide_group, slide and acquisition id (determined from the path if acquisition_dir)
    s = acquisition_dir.rsplit(os.sep, 3)
    ac = s[-1]
    sl = s[-2]
    sg = s[-3]
    df['slide_group_id'] = sg
    df['slide_id'] = sl
    df['acquisition_id'] = ac

    # add distances
    if 'd21' in df.columns:
        df['len3d'] = np.sqrt((df.d01 - df.d02)**2 + (df.d11 - df.d12)**2 + (df.d21 - df.d22)**2)
    df['len2d'] = np.sqrt((df.d01 - df.d02)**2 + (df.d11 - df.d12)**2)
    return df


def remove_analyses(acquisition_dir, analysis_check, analysis_info = 'analysis_info.json', dry_run = True,
                   remove_results = True):
    """
    remove analyses that match a certain criterion
    :param acquisition_dir: base dir of the acquisition
    :param analysis_check: predicate function that takes one element from analysis_info/analyses plus the acquisition dir
        and decides whether the analysis should be kept
    :param analysis_info: analysis info JSON file name
    :param dry_run: whether to do a dry run
    :param remove_results: whether to remove the .csv results of analyses removed from json
    :return: None
    """
    if not os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        print ('skipping ' + acquisition_dir + ', no analysis info found')
        return None
    
    with open(os.path.join(acquisition_dir, analysis_info), 'r') as fd:
        info = json.load(fd)
    
    analyses_info = recursive_dict_query(info, '/analyses')
    if analyses_info is None:
        print ('skipping ' + acquisition_dir + ', no analyses done so far for this acquisition')
        return None
    
    good_analyses = [a for a in analyses_info if analysis_check(a, acquisition_dir)]
    bad_analyses = [a for a in analyses_info if not analysis_check(a, acquisition_dir)]
    
    current_analysis = recursive_dict_query(info, '/analysis_current')
    
    current_is_failed = reduce(lambda x,y : x or (y['name'] == current_analysis), bad_analyses, False)
    
    if len(good_analyses) > 0:
        info['analysis_current'] = next(reversed(good_analyses))['name']
    else:
        if 'analysis_current' in info:
            del info['analysis_current']
            
    info['analyses'] = good_analyses
    
    if dry_run:
        print('processed ' + acquisition_dir)
        print('removed analyses:')
        print(json.dumps(bad_analyses, indent = 2))
        print('new info:')
        print(json.dumps(info, indent = 2))
    else:
        if len(bad_analyses) == 0:
            return None
                
        make_old_copy(os.path.join(acquisition_dir, analysis_info))
        with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
            json.dump(info, fd, indent = 2)
            
    for a in bad_analyses:
        if os.path.exists(os.path.join(acquisition_dir, a['name'] + '.csv')):
            if dry_run:
                print('rm ' + os.path.join(acquisition_dir, a['name'] + '.csv'))
            else:
                os.remove(os.path.join(acquisition_dir, a['name'] + '.csv'))

                
def make_result_visualization(acquisition_dir, analysis_info = 'analysis_info.json', results=None, save_results = False,
                              sample = None, out_dir = 'results_visualization', tiff_dir = 'tiff'):
    """
    display or save visualization of results
    :param acquisition_dir: the acquisition directory to process
    :param analysis_info: filename of the json analysis info
    :param results: which results to use (None indicates most recent)
    :param save_results: whether to save the results as png files (if False, results will be displayed)
    :param sample: how many of results to randomly sample, if this is None, all will be used
    :param out_dir: where to save results to (will be appended with numerical suffix)
    :param tiff_dir: subdir in which raw tiff files are stored
    :return:
    """
    
    # set umask so that group has write access
    os.umask(0o002)
    
    # skip if we want to read current results from json, but no analysis_info.json exists
    if (results is None) and (not os.path.exists(os.path.join(acquisition_dir, analysis_info))):
        print ('skipping ' + acquisition_dir + ', no analysis info found')
        return None
    
    # try to get requested analysis
    if not (results is None):
        if not (os.path.exists(os.path.join(acquisition_dir, results + '.csv'))):
            print ('skipping ' + acquisition_dir + ', requested analysis not found')
            return None
        current_analysis = results
    
    # try to get current results
    else:
        current_analysis = query_json(os.path.join(acquisition_dir, analysis_info), '/analysis_current')
        if current_analysis is None:
            print ('skipping ' + acquisition_dir + ', no analysis info found')
            return None
    
    df = pd.read_csv(os.path.join(acquisition_dir, current_analysis + '.csv'))
    
    fdict = dict()
    p = re.compile('(.*?)_ch[0-9]+\.tif')
    for d, _, fs in os.walk(os.path.join(acquisition_dir, tiff_dir)):
        for f in fs:
            m = p.match(f)
            if not (m is None):
                fdict[m.groups()[0]] = os.path.join(d, m.groups()[0])
    
    if not (sample is None):
        ser = df.sample(min(sample, df.file.size))
    else:
        ser = df
        
    if save_results:
        outdir_ = os.path.join(acquisition_dir, out_dir + '_' + current_analysis)
        mkdir_if_necessary(os.path.join(acquisition_dir, out_dir + '_' + current_analysis))
    
    tasks = []
    for idx, row in ser.iterrows():
        
        ch1file = fdict[row.file.rsplit(os.sep, 1)[-1]] + '_ch0.tif'
        ch2file = fdict[row.file.rsplit(os.sep, 1)[-1]] + '_ch1.tif'
        pos_1 = (row.d01, row.d11)
        pos_2 = (row.d02, row.d12)
        outfile = os.path.join(outdir_, row.file.rsplit(os.sep, 1)[-1] + '.png') if save_results else None
        tasks.append([save_results, ch1file, ch2file, pos_1, pos_2, outfile])
    
    
    # save old plot size, so we can reset afterwards
    oldplotsize = plt.rcParams["figure.figsize"]
    plt.rcParams["figure.figsize"] = (5,5)
    
    # save multiprocessed
    if save_results:
        pool = Pool()    
        pool.starmap(do_plot, tasks)    
        pool.close()
    
    # plots dont seem to show when we do this in separate processes, thus do not multiprocess
    else:
        for task in tasks:
            do_plot(*task)
    
    plt.rcParams["figure.figsize"] = oldplotsize

    
def do_plot(save_results, ch1file, ch2file, pos_1, pos_2, outfile):
    '''
    plot 2-color image sum projection and show position of detected spots as circles
    :param save_results: whether to save the plot or just display it
    :param ch1file: tiff file for channel1
    :param ch2file: tiff file for channel2
    :param pos_1: spot position in channel 1
    :param pos_2: spot position in channel 2
    :param outfile: file to save plot to
    :return:
    '''
    
    img1, info1 = read_image_stack(ch1file, correct16bit=True, get_info=True)
    img2 = read_image_stack(ch2file, correct16bit=True, get_info=False)
                
    # TODO: can we count on this info being there all the time?
    # resolution is pixel_size ^ -1, we assume the same size in x and y and in both channels
    resolution = info1['resolution'][0]
    
    pos_1 = (pos_1[0] * resolution, pos_1[1] * resolution)
    pos_2 = (pos_2[0] * resolution, pos_2[1] * resolution)
        
    img1 = np.array(img1, dtype=float)
    img2 = np.array(img2, dtype=float)
        
    img1 = np.apply_along_axis(np.sum, 2, img1)
    img2 = np.apply_along_axis(np.sum, 2, img2)
        
    img1 = img1 / np.max(img1)
    img2 = img2 / np.max(img2)
        
    plt.figure()
    plt.imshow(np.dstack((img1, img2, np.zeros(img1.shape))))
        
    ax = plt.gca()
    circle1 = plt.Circle(pos_1, 3, fill = False, color='white')
    circle2 = plt.Circle(pos_2, 3, fill = False, color='white')
    ax.add_artist(circle1)
    ax.add_artist(circle2)
        
    if save_results:
        plt.savefig(outfile)
        plt.close()


def scale_zero_one(img):
    '''
    scale an image from min intensity: 0 to max intensity: 1
    :param img: image to scale
    :return: np-array of the same shape as img, dtype float
    '''
    img = np.array(img, dtype=float)
    return (img - np.min(img)) / (np.max(img) - np.min(img))


def segment_nuclei(acquisition_dir, analysis_info='analysis_info.json', segmentation_name_refix='segmented_nuclei',
                   tiff_dir='tiff', hierarchy_level='field', show_overlay=False, channels=[0, 1],
                   min_circularity=.7, min_circularity_first_pass=.9, min_area=1500, max_area=7500):
    '''
    generate nuclear segmentations from overview images for an acquisition.

    the segmentation is a 2-step process:
    * we will first auto-threshold the image and detect objects in this mask
    * objects not in the size range or below the circularity threshold will be subjected to a watershed transform
        using the negative EDM of the object as elevation and maxima of the EDM as seeds

    :param acquisition_dir: the directory to process
    :param analysis_info: filename of the json analysis info
    :param segmentation_name_refix: prefix for the directory to save results to, will receive a suffix
    :param tiff_dir: dir containing raw tiff files
    :param hierarchy_level: the hierarchy level on which to segment
    :param show_overlay: whether to show results, if this is True, we will not save results
    :param channels: the channels to use
    :param min_circularity: minimum circularity of objects detected
    :param min_circularity_first_pass: minimum circularity in a first pass, we will try to chop up
        objects not meeting this criterion with a watershed transform
    :param min_area: minimum area of objects detected
    :param max_area: maximum area of objects detected (pixels**2)
    :return:
    '''
    
    # set umask so that group has write access
    os.umask(0o002)

    if not (os.path.exists(os.path.join(acquisition_dir, tiff_dir, hierarchy_level))):
        print('no tiff dir of specified level present for ' + acquisition_dir + '. skipping.')
        return

    p_seg = re.compile('.*?([0-9]+)')

    if os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        with open(os.path.join(acquisition_dir, analysis_info), 'r') as fd:
            info = json.load(fd)
    else:
        info = dict()

    segmentation_info = recursive_dict_query(info, '/segmentations')
    if segmentation_info is None:
        segmentation_info = list()

    max_idx = reduce(lambda x, y: min(x, int(p_seg.match(y['name']).groups()[0])), segmentation_info, -1)

    seg_name = segmentation_name_refix + str(max_idx + 1)

    seg_info_i = {'name': seg_name,
                  'channels': channels,
                  'min_circularity': min_circularity,
                  'min_circularity_first_pass': min_circularity_first_pass,
                  'min_area': min_area,
                  'max_area': max_area}

    segmentation_info.append(seg_info_i)
    info['segmentations'] = segmentation_info
    info['current_segmentation'] = seg_name

    p = re.compile('(.*?)_ch[0-9]+\.tif')
    p2 = re.compile('(?:.*?' + os.sep + ')+(.*?)\.msr')

    tiff_prefixes = set()
    for f in next(os.walk(os.path.join(acquisition_dir, tiff_dir, hierarchy_level)))[2]:
        m = p.match(f)
        if not (m is None):
            tiff_prefixes.add(m.groups()[0])
    tiff_prefixes = sorted(list(tiff_prefixes))
    tiff_prefixes = [os.path.join(acquisition_dir, tiff_dir, hierarchy_level, pre) for pre in tiff_prefixes]

    #    for pre in tiff_prefixes:
    #        print(pre)

    pool = Pool()
    calls = [(t, show_overlay, channels, min_circularity, min_circularity_first_pass, min_area, max_area) for t in
             tiff_prefixes]
    res = pool.starmap(_segment_nuclei_inner, calls)
    pool.close()
    #    res = [segment_nuclei_inner(*c) for c in calls]
    #    for pre in tiff_prefixes:
    #        m = p2.match(pre)
    #        if not (m is None):
    #            print(m.groups()[0])

    if show_overlay:
        for r in res:
            plt.figure()
            plt.imshow(r)

    else:
        mkdir_if_necessary(os.path.join(acquisition_dir, seg_name))

        for i in range(len(tiff_prefixes)):

            if res[i] is None:
                continue

            img, iinfo = res[i]

            save_2d_tiff(img, iinfo,
                         os.path.join(acquisition_dir, seg_name, tiff_prefixes[i].rsplit(os.sep, 1)[-1] + '.tif'))

        if os.path.exists(os.path.join(acquisition_dir, analysis_info)):
            make_old_copy(os.path.join(acquisition_dir, analysis_info))

        with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
            json.dump(info, fd, indent=2)

    print('finished ' + acquisition_dir)


def _segment_nuclei_inner(tiff_prefix, rgb=False, channels=[0, 1], min_circularity=.7, min_circularity_first_pass=.9,
                          min_area=1500, max_area=7500):
    """
    do the actual segmentation for one image
    :param tiff_prefix: prefix of the images
    :param rgb: wether to return an rgb-overlay of masks onto original image or just mask (plus some image metadata)
    :param channels: which channels to use
    :param min_circularity:
    :param min_circularity_first_pass:
    :param min_area:
    :param max_area:
    :return: rgb-np-array if rgb == True or (np-array of masks, info dict) if rgb == False
    """
    if len(channels) < 1:
        print('no channels selected')
        return None

    fs = [tiff_prefix + '_ch{}.tif'.format(c) for c in channels]
    imgs_plus_info = [read_image_stack(f, True, True) for f in fs]
    projs = [np.apply_along_axis(np.max, 2, np.array(iinfo[0], dtype=float)) for iinfo in imgs_plus_info]

    info = imgs_plus_info[0][1]

    if len(projs) == 1:
        proj = projs[0]
    else:
        proj = reduce(lambda x, y: x + y, projs) / len(projs)
    
    # image has uniform brightness, this causes errors downstream, so we return empty result here
    if np.abs(np.max(proj) - np.min(proj)) < np.finfo(float).eps:
        lab = np.zeros(proj.shape).astype(np.int)
        overlay = label2rgb(lab, image=proj, bg_label=0, alpha=.4)
        if rgb:
            return overlay
        else:
            return (lab, info)
        
    proj = scale_zero_one(ndi.gaussian_filter(proj, 2))

    t = filters.threshold_otsu(proj)
    thresholded = proj > t
    thresholded = morphology.binary_opening(thresholded, morphology.disk(2))

    lab, nlabs = ndi.label(thresholded)

    # for watershed refinement
    dt = ndi.distance_transform_edt(thresholded)

    # dt maxima with some cleanup
    pks = feature.peak_local_max(dt, exclude_border=False, min_distance=30, indices=False)

    # LoG minima a seeds for watershed
    # pks = peak_local_max(- ndi.gaussian_laplace(proj, radius / (2 * np.sqrt( 2 * np.log(2) ))), exclude_border=False,
    #                    min_distance=5, indices=False)
    pks, npeaks = ndi.label(pks, structure=morphology.rectangle(3, 3))

    # ws for everything in thresholded img
    ws = morphology.watershed(-dt, pks, mask=thresholded)

    ## first pass, remove small objects, try to watershed big or weirdly shaped objs
    rprops = measure.regionprops(lab)
    for r in rprops:
        # remove small
        if r.area < min_area:
            lab[lab == r.label] = 0
        # intermediate size
        elif r.area < max_area:
            circ = 4. * np.pi * r.area / r.perimeter / r.perimeter
            # weird shape, try watershed
            if circ < min_circularity_first_pass:
                lab[lab == r.label] = ws[lab == r.label] + np.max(lab)
                # big objects, try watershed
        else:
            lab[lab == r.label] = ws[lab == r.label] + np.max(lab)

    ## second, cleaning pass
    rprops = measure.regionprops(lab)
    for r in rprops:
        # remove small
        if r.area < min_area:
            lab[lab == r.label] = 0
        # intermediate size
        elif r.area < max_area:
            circ = 4. * np.pi * r.area / r.perimeter / r.perimeter
            # weird shape, remove
            if circ < min_circularity:
                lab[lab == r.label] = 0
        # big objects, remove
        else:
            lab[lab == r.label] = 0

    # remove objs on border and relabel from 0
    segmentation.clear_border(lab, in_place=True)
    lab, _, _ = segmentation.relabel_sequential(lab)

    overlay = label2rgb(lab, image=proj, bg_label=0, alpha=.4)
    if rgb:
        return overlay
    else:
        return (lab, info)

    
def make_sorted_projections(acquisition_dir, sorting = None, analysis_info = 'analysis_info.json',
                            jpeg_dir = 'jpeg_sum_projection', out_prefix = 'sorted_projections'):
    """
    create a directory of copies of jpeg sum projections respresenting the sorting results
    :param acquisition_dir: directory of the acquisition to process
    :param sorting: which sorting to visualize (may be None, in which case the most recent sorting is used)
    :param analysis_info: name of the analysis ticket file
    :param jpeg_dir: directory containing sum projections
    :param out_prefix: prefix of the directory the results will be saved to (will be appended with a nuerical suffix)
    :return: None
    """
    
    # set umask so that group has write access
    os.umask(0o002)
    
    
    if not os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        print('WARNING: no analysis info found for ' + acquisition_dir + '. skipping.')
        return
    
    if not (sorting is None) and not os.path.exists(os.path.join(acquisition_dir, sorting)):
        print('WARNING: selected sorting not found for ' + acquisition_dir + '. skipping.')
        return
    
    if sorting is None:
        sorting = query_json(os.path.join(acquisition_dir, analysis_info), 'sorting_current')
        if sorting is None:
            print('WARNING: current sorting not found for ' + acquisition_dir + '. skipping.')
            return
    
    with open(os.path.join(acquisition_dir, analysis_info), 'r') as fd:
        info = json.load(fd)
        
    if not ('sorted_projections' in info.keys()):
        info['sorted_projections'] = list()
     
    
    p = re.compile('.*?([0-9]+)')
    sorted_projs_info = info['sorted_projections']
    if len(sorted_projs_info) > 0:
        idxes = [int(p.match(si['name']).groups()[0]) for si in sorted_projs_info]
        max_idx = reduce(max, idxes, 0) + 1
    else:
        max_idx = 0
        
    outdir = out_prefix + str(max_idx)
    
    mkdir_if_necessary(os.path.join(acquisition_dir, outdir))
    
    class_map = dict()
    
    for d in next(os.walk(os.path.join(acquisition_dir, sorting)))[1]:
        mkdir_if_necessary(os.path.join(acquisition_dir, outdir, d))
        for f in next(os.walk(os.path.join(acquisition_dir, sorting, d)))[2]:
            class_map[f] = d
    
    
    for parent, _, f in os.walk(os.path.join(acquisition_dir, jpeg_dir)):
        for fi in f:
            f_wo_jpeg = fi.replace('.jpg', '')
            if f_wo_jpeg in class_map.keys():
                cls = class_map[f_wo_jpeg]                
                copy(os.path.join(parent, fi), os.path.join(acquisition_dir, outdir, cls, fi))
    
    new_projection_info = {'name' : outdir, 'sorting' : sorting, 'jpegs' : jpeg_dir}
    
    sorted_projs_info.append(new_projection_info)
    info['sorted_projections'] = sorted_projs_info
    info['sorted_projections_current'] = outdir
    
    make_old_copy(os.path.join(acquisition_dir, analysis_info))
    
    with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
        json.dump(info, fd, indent=2)
    
    
def sorted_projections_to_new_sorting(acquisition_dir, analysis_info = 'analysis_info.json', sorted_projections = None,
                                     raw_dir = 'raw', out_prefix = 'sorted'):
    
    # set umask so that group has write access
    os.umask(0o002)
    """
    create a new sorting (directory with sorted links to raw data) and register it in analysis_info file
    :param acquisition_dir: directory of the acquisition to process
    :param analysis_info: name of the analysis ticket file
    :param sorted_projections: which sorted (and manually re-sorted) projections to generate sorting from
            (may be None, in which case the most recent sorted projections are used)
    :param raw_dir: (unsorted) raw data directory
    :param out_prefix: prefix of the directory the results will be saved to (will be appended with a nuerical suffix)
    :return: None
    """
    if not os.path.exists(os.path.join(acquisition_dir, analysis_info)):
        print('WARNING: no analysis info found for ' + acquisition_dir + '. skipping.')
        return
    
    if not (sorted_projections is None) and not os.path.exists(os.path.join(acquisition_dir, sorted_projections)):
        print('WARNING: selected sorting not found for ' + acquisition_dir + '. skipping.')
        return
    
    if sorted_projections is None:
        sorted_projections = query_json(os.path.join(acquisition_dir, analysis_info), 'sorted_projections_current')
        if sorted_projections is None:
            print('WARNING: current sorting not found for ' + acquisition_dir + '. skipping.')
            return
    
    with open(os.path.join(acquisition_dir, analysis_info), 'r') as fd:
        info = json.load(fd)
        
    if not ('sorting' in info.keys()):
        info['sorting'] = list()
     
    
    p = re.compile('.*?([0-9]+)')
    sorting_info = info['sorting']
    if len(sorting_info) > 0:
        idxes = [int(p.match(si['name']).groups()[0]) for si in sorting_info]
        max_idx = reduce(max, idxes, 0) + 1
    else:
        max_idx = 0
        
    outdir = out_prefix + str(max_idx)    
    mkdir_if_necessary(os.path.join(acquisition_dir, outdir))
    
    class_map = dict()
    
    for d in next(os.walk(os.path.join(acquisition_dir, sorted_projections)))[1]:
        mkdir_if_necessary(os.path.join(acquisition_dir, outdir, d))
        for f in next(os.walk(os.path.join(acquisition_dir, sorted_projections, d)))[2]:
            class_map[f] = d
            
    for parent, _, f in os.walk(os.path.join(acquisition_dir, raw_dir)):
        for fi in f:
            f_w_jpeg = fi + '.jpg'
            if f_w_jpeg in class_map.keys():
                cls = class_map[f_w_jpeg]               
                dir_dest = os.path.join(os.path.join(acquisition_dir, outdir), cls)
                link_src = os.path.join(parent, fi)
                link_src = os.path.relpath(link_src, dir_dest)
                symlink_if_necessary(os.path.join(dir_dest, fi ), link_src)
    
    new_sorting_info = {'name' : outdir, 'manually_sorted_projections' : sorted_projections, 'type' : 'manual'}
    
    sorting_info.append(new_sorting_info)
    info['sorting'] = sorting_info
    info['sorting_current'] = outdir
    
    make_old_copy(os.path.join(acquisition_dir, analysis_info))
    
    with open(os.path.join(acquisition_dir, analysis_info), 'w') as fd:
        json.dump(info, fd, indent=2)