# Pipeline tests

The notebooks in this directory contain mainly testing code calling various steps of the pipeline by hand.

* ```test_call_analysis.ipynb``` contains snippets for doing analysis / getting results and removing results
* ```directory_structure_correction.ipynb``` contains code to re-organize old acquisitions to conform to our new data structure
* ```nuclear_segementation_test.ipynb``` contains test code for nuclear segmentation in overview images
* ```classification_sorting_test.ipynb``` contains snippets from getting features/training classifiers and using them to sort images